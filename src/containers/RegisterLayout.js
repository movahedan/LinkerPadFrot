import React, { Component } from 'react'
import LogoComponent from '../components/account/LogoComponent'



class RegisterLayout extends Component {
    render() {
        return (
            <div className="app flex-row align-items-center h-auto bg-light">

                {/* background */}
                <div className="col-md-7 bg-auth d-none d-md-block"></div>

                <div className="col-md-5 col-12 dir-rtl">
                    <div className="row align-items-start justify-content-center">

                        <LogoComponent />
                        
                        {/* children of layout */}
                        {this.props.children}

                    </div>
                </div>

            </div>
        )
    }
}


export default RegisterLayout;