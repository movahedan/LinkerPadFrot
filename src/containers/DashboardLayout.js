import React, { Component } from "react";
import HeaderLayoutComponent from "../components/layout/HeaderLayoutComponent";

class DashboardLayout extends Component {
  render() {
    return (
      <div className="container-fluid">
        <HeaderLayoutComponent />
        <div className="animated fadeIn">{this.props.children}</div>
      </div>
    );
  }
}

export default DashboardLayout;
