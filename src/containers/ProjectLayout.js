import React, { Component } from 'react'
import DashboardLayout from './DashboardLayout'


class ProjectLayout extends Component {


    render() {
        return (
            <DashboardLayout>

                { this.props.children }

            </DashboardLayout>
        )
    }

}


export default ProjectLayout