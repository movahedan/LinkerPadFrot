import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { toast } from 'react-toastify'
import RegisterLayout from '../../containers/RegisterLayout'
import InputTextComponent from '../../components/form/InputTextComponent'
import AccountController from '../../controllers/AccountController'
import RegisterViewModel from '../../view_models/account/RegisterViewModel'
import RegisterModel from '../../api_models/account/RegisterModel'
import registerValidation from '../../validations/account/registerValidation'

class RegisterView extends Component {


    constructor(props) {
        super(props)
        this.state = {
            values: new RegisterViewModel().state,
        options: { redirect: false }
        }
    }


    onChange(e) {
        const { name, value } = e.target
        const { values } = this.state
        values[name] = value
        this.setState({ values })
    }


    onSubmit(e) {
        e.preventDefault()

        const { values, options } = this.state
        const registerModel = new RegisterModel(values)

        registerValidation(this, values)
            .then(() => {
                return AccountController.register(registerModel)
            })
            .then(() => {
                return AccountController.sendConfirmationToken(registerModel)
            })
            .then(() => {
                options.redirect = true
                this.setState({ options })
            })
            .catch(error => {
                if(typeof error === "object")
                {
                    toast.error(error.message)
                }
            })
    }


    render() {

        const { values, options } = this.state;

        if (options.redirect === true) {
          return (
            <Redirect to={{
              pathname: '/verification',
              state: { mobileNumber: values.mobileNumber, action: 'login' }
            }} />
          )
        }

        return (
          <RegisterLayout>

            <div className="col-12 my-4">
              <h2 className="text-center">ثبت نام</h2>
            </div>

            <form className="col-10 my-4" onSubmit={this.onSubmit.bind(this)}>
              <div className="row">

                <InputTextComponent
                  name="firstName"
                  value={values.firstName}
                  parentInputClass="col-12 col-sm-6 my-3"
                  inputClass="form-control dir-rtl"
                  placeholder="نام"
                  onChange={this.onChange.bind(this)}
                  ref={(node) => this.firstNameField = node}
                />

                <InputTextComponent
                  name="lastName"
                  value={values.lastName}
                  parentInputClass="col-12 col-sm-6 my-3"
                  inputClass="form-control dir-rtl"
                  placeholder="نام خانوادگی"
                  onChange={this.onChange.bind(this)}
                  ref={(node) => this.lastNameField = node}
                />

                <InputTextComponent
                  name="mobileNumber"
                  value={values.mobileNumber}
                  parentInputClass="col-12 my-3"
                  inputClass="form-control pr-5"
                  placeholder="تلفن همراه (۰۹۱۱۰۰۰۰۰۰۰)"
                  onChange={this.onChange.bind(this)}
                  ref={(node) => this.mobileNumberField = node}
                >
                  <i className="fa fa-mobile-phone fa-2x  position-absolute text-muted"
                    style={{ top: "3px", right: "35px" }}>
                  </i>
                </InputTextComponent>

                <InputTextComponent
                  name="password"
                  type="password"
                  value={values.password}
                  parentInputClass="col-12 my-3"
                  inputClass="form-control pr-5"
                  placeholder="رمز عبور"
                  onChange={this.onChange.bind(this)}
                  ref={(node) => this.passwordField = node}
                >
                  <i className="fa-lg icon-key position-absolute text-secondary text-muted"
                    style={{ top: "7px", right: "32px" }}>
                  </i>
                </InputTextComponent>

                <div className="col-12 my-3">
                  <input type="submit" className="btn btn-primary form-control" value="ثبت نام" />
                </div>

              </div>
            </form>

            <div className="col-10 text-right">
                <p>قبلا ثبت نام کرده اید؟ <Link to="/login">ورود به لینکرپد</Link></p>
            </div>

          </RegisterLayout>
        );
    }
}

export default RegisterView
