import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { toast } from 'react-toastify'
import { connect } from 'react-redux';
import RegisterLayout from '../../containers/RegisterLayout'
import AccountController from '../../controllers/AccountController'
import VerificationCodeTextBoxComponent from '../../components/account/VerificationCodeTextBoxComponent'
import CountDownTimerComponent from '../../components/account/CountDownTimerComponent'
import VerificationModel from '../../api_models/account/VerificationModel'
import VerificationViewModel from '../../view_models/account/VerificationViewModel'
import { toggleDisplayResendVerificationCodeLink } from '../../actions/optionAction'


class VerificationView extends Component {

    constructor(props) {
        super(props)
        this.state = {
            values: new VerificationViewModel().state,
            options: {
                redirect: false,
                url: '/login',
                action: ''
            }
        }
    }


    componentWillMount() {
        const { state } = this.props.location
        const { values, options } = this.state
        if(!state || !state.mobileNumber || !state.action) {
            options.redirect = true
            this.setState({ options })
        } else {
            switch(state.action) {
                case 'login':
                    options.url = '/login'
                    break
                case 'changePassword':
                    options.url = '/set-new-password'
                    break
                case 'confirmMobileNumber':
                    options.url = '/login'
                    break
                default:
                    options.url = '/login'
            }
            options.action = state.action
            values.mobileNumber = state.mobileNumber
            this.setState({ values, options })
        }
    }


    onSubmit(e) {
        e.preventDefault()
        const { values, options } = this.state
        const formValidate = this.verificationCodeField.checkValiationNumbers()
        if(!formValidate) {
            return
        }

        const verificationModel = new VerificationModel(values)
        if(options.action === 'changePassword') {
            AccountController.resetPasswordToken(verificationModel)
                .then(resetPasswordToken => {
                    values.resetPasswordToken = resetPasswordToken
                    options.redirect = true
                    this.setState({ values, options })
                })
                .catch( error => toast.error(error.message))
        } else {
            AccountController.confirmToken(verificationModel)
                .then(() => {
                    options.redirect = true
                    this.setState({ options })
                    if(options.action === 'confirmMobileNumber') {
                        toast.success('شماره شما تایید شد')
                    }
                })
                .catch( error => toast.error(error.message))
        }
    }


    handleUpdateToken(token) {
        const { values } = this.state
        values.token = token
        this.setState({ values })
    }


    handleSendConfimationToken() {
        const { values } = this.state
        const verificationModel = new VerificationModel(values)
        AccountController.sendConfirmationToken(verificationModel)
            .then(() => {
                this.props.toggleDisplayResendVerificationCodeLink(false)
                this.countDownTimerField.start()
            })
            .catch( error => {
                toast.error(error.message)
            })
    }


    render() {

        const { options, values } = this.state
        const { displayResendVerificationCodeLink } = this.props

        if(options.redirect === true) {
            return (
                <Redirect to={{
                    pathname: options.url,
                    state: { mobileNumber: values.mobileNumber, resetPasswordToken: values.resetPasswordToken }
                }} />
            )
        }

        return (
            <RegisterLayout>
                
                <div className="col-12 my-4">
                    <h2 className="text-center">فعال سازی حساب کاربری</h2>
                </div>

                <div className="col-10 my-4">
                    <h6 className="text-right">کد ۵ رقمی ارسال شده به شماره {values.mobileNumber} را وارد کنید</h6>
                </div>

                <form className="col-10 my-4" onSubmit={this.onSubmit.bind(this)}>

                    <VerificationCodeTextBoxComponent 
                        ref={ (node) => {this.verificationCodeField = node} }
                        handleUpdateToken={this.handleUpdateToken.bind(this)} 
                    />

                    <div className="form-row my-4">
                        <input tabIndex="6" ref="n6" type="submit" className="btn btn-primary form-control" value="تایید" />
                    </div>

                </form>

                <div className="col-10">
                    <div className="row">

                        <div className="col-6 text-right">
                            <button 
                                type="button" 
                                className="btn btn-link py-0" 
                                hidden={ !displayResendVerificationCodeLink }
                                onClick={this.handleSendConfimationToken.bind(this)}>
                                دریافت مجدد کد تایید
                            </button>
                        </div>

                        <div className="col-6 text-left">
                            <CountDownTimerComponent ref={ node => this.countDownTimerField = node } />
                        </div>
                        
                    </div>
                </div>

            </RegisterLayout>
        )
    }
}


const mapStateToProps = state => ({
    displayResendVerificationCodeLink: state.options.displayResendVerificationCodeLink
})
    

const mapDispatchToProps = dispatch => ({
    toggleDisplayResendVerificationCodeLink: status => {
        dispatch(toggleDisplayResendVerificationCodeLink(status))
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(VerificationView)
