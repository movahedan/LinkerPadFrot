export default ({values, handleChange, errors}) => [
  [
    {
      label: "نام",
      name: "firstName",
      value: values.firstName,
      onChange: handleChange,
      type: "text",
      placeholder: "لطفا نام خود را وارد کنید",
      error: errors.firstName
    },
    {
      label: "نام خانوادگی",
      name: "lastName",
      value: values.lastName,
      onChange: handleChange,
      type: "text",
      placeholder: "لطفا نام خانوادگی خود را وارد کنید",
      error: errors.lastName
    }
  ],
  [
    {
      label: "شرکت",
      name: "company",
      value: values.company,
      onChange: handleChange,
      type: "text",
      placeholder: "لطفا نام شرکت خود را وارد کنید"
    },
    {
      label: "تخصص",
      name: "skill",
      value: values.skill,
      onChange: handleChange,
      type: "text",
      placeholder: "لطفا تخصص های خود را وارد کنید"
    }
  ],
  // [
  //   {
  //     label: "اعتبار",
  //     name: "firstName",
  //     value: "",
  //     onChange: e => console.log(e.target.value),
  //     type: "text",
  //     placeholder: "اعتبار شما",
  //     disabled: true,
  //     className: "mx-auto"
  //   }
  // ]
];