import React, { useState, useEffect } from "react";

import useAccount from "stores/account/useAccount";
import ProjectLayout from "containers/ProjectLayout";
import messages from "utils/validationMessage/messages";

import ProfileUploadSectionComponent from "./ProfileUploadSection";
import ChangePasswordModalComponent from "./ChangePasswordModal";
import ButtonContainer from "./ButtonContainer";
import contentField from "./formFieldsContent";
import validate from "./validateFn";

import { Row, Col, Form, Button, Container } from "react-bootstrap";
import localConfigs from "local-configs";

import "./style.scss";
import { toast } from "react-toastify";
import useForm from "utils/formValidation/useForm";

const BasicInformationTextField = ({ label, error, ...props }) => {
  return (
    <>
      <Form.Label className="float-right">
        <h4>{label}</h4>
      </Form.Label>
      <Form.Control {...props} className={error ? "is-invalid" : null} />
      {error ? <span className="text-danger">{error}</span> : null}
    </>
  );
};

export default () => {
  const { account, dispatch } = useAccount();
  // const [state, setState] = useState(account);
  const [croppedImage, setCroppedImage] = useState(null);
  const [changePasswordModal, setChangePasswordModal] = useState(false);

  const onSubmit = async props => {
    await dispatch.editInformation.exec(props);
    if (!dispatch.editInformation.error) {
      console.log("test", dispatch.editInformation.error);
      toast.success(messages.success.ProfileEditedSuccessfully);
      await dispatch.getInformation.exec();
    }
  };

  const { handleChange, handleSubmit, values, setValues, errors } = useForm(
    account,
    validate,
    onSubmit
  );

  useEffect(() => {
    // setState({
    //   ...state,
    //   ...account
    // });
    setValues({ ...values, ...account });
    if (account && account.profilePictureAddress)
      setCroppedImage(localConfigs.baseUrl + account.profilePictureAddress);
  }, [account]);

  const revertChanges = () => {
    setValues({ ...values, ...account });
    setCroppedImage(
      account.profilePictureAddress
        ? localConfigs.baseUrl + account.profilePictureAddress
        : null
    );
    toast.success(messages.success.ProfileChangesRevertedSuccessfully);
  };

  const submitThePicture = async file => {
    if (file !== null) {
      await dispatch.editProfilePicture.exec(
        new File([file], "image.jpg", { type: "image/jpeg" })
      );
      setCroppedImage(URL.createObjectURL(file));
      toast.success(messages.success.ProfilePictureUploadedSuccessfully);
      await dispatch.getInformation.exec();
    }
  };

  const removeThePicture = async () => {
    await dispatch.removeProfilePicture.exec();
    setCroppedImage(null);
    toast.success(messages.success.ProjectPictureDeletedSuccessfully);
  };

  if (dispatch.getInformation.loading && account.firstName === "")
    return "لطفا منتظر بمانید";
  if (dispatch.getInformation.error) {
    return "مشکلی پیش آمده است";
  }

  return (
    <ProjectLayout>
      <ChangePasswordModalComponent
        show={changePasswordModal}
        toggle={() => setChangePasswordModal(!changePasswordModal)}
        payload={dispatch.changePassword}
      />
      <Form onSubmit={handleSubmit} className="p-3 profile-view dir-rtl container">
        <Row className="p-3 profile-view dir-rtl">
          <Col xs={12} md={8} lg={4} className="text-right">
            <ProfileUploadSectionComponent
              image={croppedImage}
              setImage={submitThePicture}
              loading={dispatch.editProfilePicture.loading}
              deletePicture={removeThePicture}
            />
            <div className="d-none d-lg-block">
              <ButtonContainer
                loading={
                  dispatch.editInformation.loading ||
                  dispatch.editProfilePicture.loading ||
                  dispatch.removeProfilePicture.loading
                }
                disabled={Object.keys(errors).length !== 0}
                handleSubmit={handleSubmit}
                revertChanges={revertChanges}
              />
            </div>
          </Col>
          <Form.Group
            as={Col}
            xs={12}
            md={10}
            lg={8}
            controlId="formPlaintextEmail"
          >
            {contentField({ values, errors, handleChange }).map((row, idx) => {
              return (
                <Row key={idx}>
                  {row.map(col => {
                    return (
                      <Col
                        key={col.name}
                        xs={12}
                        md={6}
                        className={`p-3 ${col.className}`}
                      >
                        <BasicInformationTextField
                          value={col.value}
                          name={col.name}
                          label={col.label}
                          onChange={col.onChange}
                          type={col.type}
                          placeholder={col.placeholder}
                          disabled={col.disabled}
                          error={col.error}
                        />
                      </Col>
                    );
                  })}
                </Row>
              );
            })}

            <Row>
              <Col xs={12} md={6} className="p-3">
                <Form.Label className="float-right">
                  <h4>شماره تلفن همراه</h4>
                </Form.Label>
                <Button
                  block
                  variant="outline-secondary"
                  className="d-flex box-shadow-none"
                  onClick={() => setChangePasswordModal(true)}
                >
                  <span className="ml-auto mr-0 text-dark">
                    {values.mobileNumber}
                  </span>
                  <span className="mr-auto ml-0 text-dark">تغییر</span>
                </Button>
              </Col>
              <Col xs={12} md={6} className="p-3">
                <Form.Label className="float-right w-100 d-flex align-items-center">
                  <h4 className="d-inline-block">رمز عبور </h4>
                </Form.Label>
                <Button
                  block
                  variant="outline-secondary"
                  className="d-flex box-shadow-none"
                  onClick={() => setChangePasswordModal(true)}
                >
                  <span className="ml-auto mr-0">xxxxxxxx</span>
                  <span className="mr-auto ml-0 text-dark">تغییر</span>
                </Button>
              </Col>
            </Row>
            <div className="d-block d-lg-none">
              <ButtonContainer
                loading={
                  dispatch.editInformation.loading ||
                  dispatch.editProfilePicture.loading ||
                  dispatch.removeProfilePicture.loading
                }
                disabled={Object.keys(errors).length !== 0}
                handleSubmit={handleSubmit}
                revertChanges={revertChanges}
              />
            </div>
          </Form.Group>
        </Row>
      </Form>
    </ProjectLayout>
  );
};
