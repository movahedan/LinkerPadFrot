import React, { Component } from "react";

import { CropperImageComponent } from "components/form/CropperImageComponent";
import cropImage from "utils/cropImage";

import { Modal, Button } from "react-bootstrap";

export default class CropImageModalComponent extends Component {
  state = { blobImageUrl: "", croppedAreaPixelsOfImage: {} };

  shouldComponentUpdate(newProps, newState) {
    if (newProps !== this.props || newState !== this.state) return true;
  }

  componentDidMount() {
    const { payload } = this.props.state;
    if (payload && payload !== this.state.payload) {
      const blobImageUrl = URL.createObjectURL(payload);
      this.setState({ blobImageUrl, ProfilePicture: payload });
    }
  }

  handleCropAndUploadImage = async () => {
    const { blobImageUrl, croppedAreaPixelsOfImage } = this.state;
    const { croppedImageUrl, croppedImageFile } = await cropImage(
      blobImageUrl,
      croppedAreaPixelsOfImage
    );

    this.props.setCroppedImage(croppedImageFile)
    this.props.toggle();
  };

  render() {
    const { blobImageUrl } = this.state;

    return (
      <Modal show={this.props.state.open} onHide={this.props.toggle}>
        <Modal.Header closeButton>
          <Modal.Title>
            <h5 id="cropImage">تغییر سایز عکس پروژه</h5>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ height: "50vh" }}>
          <CropperImageComponent
            imageUrl={blobImageUrl}
            setCroppedAreaPixelsOfImage={croppedAreaPixelsOfImage =>
              this.setState({ croppedAreaPixelsOfImage })
            }
          />
        </Modal.Body>
        <Modal.Footer className="p-0 mt-3">
          <div className="col px-0 mx-0">
            <Button
              type="button"
              className="btn btn-secondary h-100 w-100 border-radius-none text-white"
              data-dismiss="modal"
              onClick={this.props.toggle}
            >
              لغو
            </Button>
          </div>
          <div className="col px-0 mx-0">
            <Button
              type="button"
              className="btn btn-primary h-100 w-100 border-radius-none"
              data-dismiss="modal"
              onClick={() => {
                this.props.toggle();
                this.props.handleOpenFileBrowser();
              }}
            >
              انتخاب عکس جدید
            </Button>
          </div>
          <div className="col px-0 mx-0">
            <Button
              type="button"
              className="btn btn-success h-100 w-100 border-radius-none"
              onClick={this.handleCropAndUploadImage.bind(this)}
            >
              ثبت
            </Button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}