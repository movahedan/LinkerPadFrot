import React from "react";
import { Button } from "react-bootstrap";

export default ({ loading, disabled, handleSubmit, revertChanges }) => (
  <div className="submit-button-container w-100 d-flex p-3">
    <Button
      variant={"success"}
      type={"submit"}
      onClick={e => {
        handleSubmit();
        e.preventDefault();
      }}
      disabled={loading || disabled}
    >
      <i className="far fa-save ml-2" />
      {loading ? "در حال ثبت..." : "ثبت"}
    </Button>
    {/* <Button
      variant={"outline-secondary"}
      onClick={() => revertChanges()}
      disabled={loading}
    >
      لغو
    </Button> */}
  </div>
);