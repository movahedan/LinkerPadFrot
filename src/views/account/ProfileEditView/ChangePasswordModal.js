import React, { useState, useEffect, useRef } from "react";
import { Form, Modal, Button, InputGroup } from "react-bootstrap";
import { toast } from "react-toastify";

export default ({ show, toggle, payload }) => {
  const { data, exec, loading, error } = payload;

  const onChangePasswordSubmit = async props => await exec(props);

  const errorShownRef = useRef(true);

  const [currentPasswordValue, setCurrentPasswordValue] = useState("");
  const [currentPasswordError, setCurrentPasswordError] = useState(undefined);
  const [currentPassword‌Seen, setCurrentPassword‌Seen] = useState(false);
  const [seeRawCurrentPassword, setSeeRawCurrentPassword] = useState(false);

  const [passwordValue, setPasswordValue] = useState("");
  const [passwordError, setPasswordError] = useState(undefined);
  const [password‌Seen, setPasswordSeen] = useState(false);
  const [seeRawPassword, setSeeRawPassword] = useState(false);

  const [verifyPasswordValue, setVerifyPasswordValue] = useState("");
  const [verifyPasswordError, setVerifyPasswordError] = useState(undefined);
  const [verifyPasswordSeen, setVerifyPasswordSeen] = useState(false);
  const [seeRawVerifyPassword, setSeeRawVerifyPassword] = useState(false);

  const handleHide = () => {
    setCurrentPasswordValue("");
    setCurrentPasswordError(undefined);
    setCurrentPassword‌Seen(false);

    setPasswordValue("");
    setPasswordError(undefined);
    setPasswordSeen(false);

    setVerifyPasswordValue("");
    setVerifyPasswordError(undefined);
    setVerifyPasswordSeen(false);

    toggle();
  };

  useEffect(() => {
    if (data && data.status === "Success") {
      toast.success("رمز عبور شما با موفقیت تغییر کرد.");
      handleHide();
    }
  }, [data]);

  useEffect(() => {
    if (currentPasswordValue.length < 6 && currentPassword‌Seen)
      setCurrentPasswordError("رمز عبور کوتاه تر از ۶ کاراکتر است");
    else if (
      error &&
      error.response &&
      error.response.status === 409 &&
      errorShownRef.current
    ) {
      errorShownRef.current = false;
      setCurrentPasswordError("رمز عبور وارد شده اشتباه است.");
    } else setCurrentPasswordError(null);
  }, [currentPasswordValue, currentPassword‌Seen, error]);

  useEffect(() => {
    if (passwordValue.length < 6 && password‌Seen)
      setPasswordError("رمز عبور کوتاه تر از ۶ کاراکتر است");
    else setPasswordError(null);

    if (verifyPasswordValue.length < 6 && verifyPasswordSeen)
      setVerifyPasswordError("رمز عبور کوتاه تر از ۶ کاراکتر است");
    else setVerifyPasswordError(null);

    if (
      verifyPasswordValue !== passwordValue &&
      verifyPasswordSeen &&
      password‌Seen
    )
      setVerifyPasswordError("تکرار رمز عبور با رمز عبور جدید مطابقت ندارد");
    else setVerifyPasswordError(null);
  }, [passwordValue, verifyPasswordValue, password‌Seen, verifyPasswordSeen]);

  return (
    <Modal dir="rtl" show={show} onHide={handleHide}>
      <Modal.Header closeButton>
        <Modal.Title>
          <h5>تغییر رمز عبور</h5>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ minHeight: "30vh", marginBottom: "1rem" }}>
        <Form.Label className="float-left">
          <h6>رمزعبور فعلی</h6>
        </Form.Label>
        <InputGroup className="mb-3">
          <Form.Control
            value={currentPasswordValue}
            onChange={e => setCurrentPasswordValue(e.target.value)}
            onBlur={() => setCurrentPassword‌Seen(true)}
            type={seeRawCurrentPassword ? "text" : "password"}
            className={
              currentPassword‌Seen
                ? currentPasswordError
                  ? "is-invalid"
                  : "is-valid"
                : null
            }
            placeholder="رمز عبور فعلی خود را وارد کنید"
          />
          <InputGroup.Append>
            <Button
              variant="outline-secondary"
              onClick={() => setSeeRawCurrentPassword(!seeRawCurrentPassword)}
            >
              {!seeRawCurrentPassword ? (
                <i className="far fa-eye-slash vertical-align-middle" />
              ) : (
                <i className="far fa-eye vertical-align-middle" />
              )}
            </Button>
          </InputGroup.Append>
        </InputGroup>
        {currentPassword‌Seen && currentPasswordError !== undefined ? (
          <span className="text-danger">{currentPasswordError}</span>
        ) : null}
        <Form.Label className="float-left mt-3">
          <h6>رمز عبور جدید</h6>
        </Form.Label>
        <InputGroup className="mb-3">
          <Form.Control
            value={passwordValue}
            onChange={e => setPasswordValue(e.target.value)}
            type="password"
            onBlur={() => setPasswordSeen(true)}
            type={seeRawPassword ? "text" : "password"}
            className={
              password‌Seen ? (passwordError ? "is-invalid" : "is-valid") : null
            }
            placeholder="لطفا رمز عبور جدید را وارد کنید"
          />
          <InputGroup.Append>
            <Button
              variant="outline-secondary"
              onClick={() => setSeeRawPassword(!seeRawPassword)}
            >
              {!seeRawPassword ? (
                <i className="far fa-eye-slash vertical-align-middle" />
              ) : (
                <i className="far fa-eye vertical-align-middle" />
              )}
            </Button>
          </InputGroup.Append>
        </InputGroup>
        {password‌Seen && passwordError !== undefined ? (
          <span className="text-danger">{passwordError}</span>
        ) : null}
        <Form.Label className="float-left mt-3">
          <h6>تکرار رمز عبور</h6>
        </Form.Label>
        <InputGroup className="mb-3">
          <Form.Control
            value={verifyPasswordValue}
            onChange={e => setVerifyPasswordValue(e.target.value)}
            onBlur={() => setVerifyPasswordSeen(true)}
            type={seeRawVerifyPassword ? "text" : "password"}
            className={
              verifyPasswordSeen
                ? verifyPasswordError
                  ? "is-invalid"
                  : "is-valid"
                : null
            }
            placeholder="لطفا دوباره رمز عبور جدید را وارد کنید"
          />
          <InputGroup.Append>
            <Button
              variant="outline-secondary"
              onClick={() => setSeeRawVerifyPassword(!seeRawVerifyPassword)}
            >
              {!seeRawVerifyPassword ? (
                <i className="far fa-eye-slash vertical-align-middle" />
              ) : (
                <i className="far fa-eye vertical-align-middle" />
              )}
            </Button>
          </InputGroup.Append>
        </InputGroup>

        {verifyPasswordSeen && verifyPasswordError !== undefined ? (
          <span className="text-danger">{verifyPasswordError}</span>
        ) : null}
      </Modal.Body>
      <Modal.Footer className="p-0 mt-3">
        <div className="col px-0 mx-0">
          <Button
            type="button"
            className="btn btn-success h-100 w-100 border-radius-none"
            onClick={() => {
              errorShownRef.current = true;
              onChangePasswordSubmit({
                currentPassword: currentPasswordValue,
                newPassword: passwordValue
              });
            }}
            disabled={
              currentPasswordError ||
              passwordError ||
              verifyPasswordError ||
              !currentPassword‌Seen ||
              !password‌Seen ||
              !verifyPasswordSeen ||
              loading
            }
          >
            ثبت {loading && "در حال"}
          </Button>
        </div>
        <div className="col px-0 mx-0">
          <Button
            type="button"
            className="btn btn-secondary h-100 w-100 border-radius-none text-white"
            data-dismiss="modal"
            onClick={handleHide}
          >
            لغو
          </Button>
        </div>
      </Modal.Footer>
    </Modal>
  );
};
