import * as yup from "yup";

const needValidation = ['firstName', 'lastName'];

export default async values => {
  let errors = {};
  await Promise.all(Object.keys(values).map(async key => {
    if(needValidation.indexOf(key) !== -1) {
      let schema = yup.string().required();
  
      errors = await schema.isValid(values[key])
        ? errors
        : { ...errors, [key]: "این بخش ضروری می باشد" };
    }
  }));
  return errors;
};