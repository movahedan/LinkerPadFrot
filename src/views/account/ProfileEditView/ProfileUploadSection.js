import React, { useState, useEffect, useRef } from "react";
// import { toast } from 'react-toastify'
import CropImageModalComponent from "views/account/ProfileEditView/CropImageModal";
import { toast } from "react-toastify";

export default ({ image, setImage, loading, deletePicture }) => {
  //   const { account, dispatch } = useAccount();
  const [openFileBrowser, setOpenFileBrowser] = useState(false);
  const [openCropperModal, setOpenCropperModal] = useState({
    open: false,
    payload: null
  });
  const closeImageElement = useRef(null);
  const projectPictureElement = useRef(null);
  const FileBrowserTogglerElement = useRef(null);

  useEffect(() => {
    let projectPictureClassName = "",
      closeImageClassName =
        "fa fa-times-circle fa-2x text-secondary remove-image-icon ";

    if (image) {
      console.log(image)
      projectPictureClassName += "image-upload-section";
      projectPictureElement.current.style.backgroundImage = `url(${image})`;
      projectPictureElement.current.innerHTML = "";
    } else {
      projectPictureElement.current.style.backgroundImage =
        "url(./../../assets/img/user-logo.svg)";
      projectPictureClassName += "profile upload-section ";
      closeImageClassName += "invisible";
    }

    projectPictureElement.current.className = projectPictureClassName;
    closeImageElement.current.className = closeImageClassName;
  }, [image]);

  const handleOpenFileBrowser = () => {
    FileBrowserTogglerElement.current.value = "";
    FileBrowserTogglerElement.current.click();
    setOpenFileBrowser(!openFileBrowser);
  };

  const onChange = e => {
    if (e.target.files[0].type.split("/")[0] !== "image")
      toast.error("فرمت فایل انتخاب شده تصویری نمی باشد.");
    else {
      const projectPicture = e.target.files[0];
      setOpenCropperModal({ open: true, payload: projectPicture });
    }
  };


  return (
    <>
      {openCropperModal.open && (
        <CropImageModalComponent
          state={openCropperModal}
          toggle={() => {
            FileBrowserTogglerElement.current.value = "";
            setOpenCropperModal({ open: false, payload: null });
          }}
          setCroppedImage={setImage}
          handleOpenFileBrowser={handleOpenFileBrowser}
        />
      )}
      <div className="row">
        <div className="col p-5">
          <span ref={closeImageElement} onClick={deletePicture} />
          <div className={`profilePictureWrapper circular not-loading ${loading ? "loading" : null}`}>
            <div className={`${loading ? "loading" : null}`} />
            <div ref={projectPictureElement} onClick={handleOpenFileBrowser} />
          </div>
          <input
            id="projectPicture"
            name="projectPicture"
            type="file"
            accept="image/png, image/jpeg"
            multiple={false}
            className="d-none"
            ref={FileBrowserTogglerElement}
            onChange={onChange}
          />
        </div>
      </div>
    </>
  );
};
