import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { toast } from 'react-toastify'
import { connect } from 'react-redux'
import { toogleDisplayVerifyMobileNumberComponent } from '../../actions/optionAction'
import RegisterLayout from '../../containers/RegisterLayout'
import InputTextComponent from '../../components/form/InputTextComponent'
import AccountController from '../../controllers/AccountController'
import LoginModel from '../../api_models/account/LoginModel'
import LoginViewModel from '../../view_models/account/LoginViewModel'
import loginValidation from '../../validations/account/loginValidation'
import VerifyMobileNumberComponent from '../../components/account/VerifyMobileNumberComponent'


class LoginView extends Component {


  constructor(props) {
    super(props)

    this.state = {
        values: new LoginViewModel().state,
        options: { redirect: false }
    }
  }


  componentDidMount() {
    const { state } = this.props.location
    if (state !== undefined && state.success) {
      toast.success(state.message)
    }
  }


  componentWillUnmount() {
    this.props.handleDisplayVerifyMobileNumberComponent(false)
  }


  onChange(e) {
    const { name, value } = e.target;
    const { values } = this.state;
    values[name] = value;
    this.setState({ values });
  }


  onSubmit(e) {
    e.preventDefault();


    const { values, options } = this.state
    const loginModel = new LoginModel(values)

    loginValidation(this, values)
        .then(() => {
            return AccountController.login(loginModel)
        })
        .then(result => {

            localStorage.setItem("token", result.responseObject.token)

            options.redirect = true
            this.setState({ options })
        })
        .catch(error => {
            
            if(error.httpStatus === 405) {
                this.props.handleDisplayVerifyMobileNumberComponent(true)
            }

            if(typeof error === "object")
            {
                toast.error(error.message)
            }
        })

  }


  render() {

    const { values, options } = this.state;

    if (options.redirect === true) {
      return (
        <Redirect to={{
          pathname: '/project',
        }} />
      )
    }

    return (
      <RegisterLayout>

        <div className="col-12 my-4">
          <h2 className="text-center">ورود</h2>
        </div>

        <form className="col-10 my-4" onSubmit={this.onSubmit.bind(this)}>
          <div className="row">

            <InputTextComponent
              name="mobileNumber"
              value={values.mobileNumber}
              parentInputClass="col-12 my-3"
              inputClass="form-control pr-5"
              placeholder="تلفن همراه (۰۹۱۱۰۰۰۰۰۰۰)"
              onChange={this.onChange.bind(this)}
              ref={(node) => this.mobileNumberField = node}
            >
              <i className="fa fa-mobile-phone fa-2x  position-absolute text-muted"
                style={{ top: "3px", right: "35px" }}>
              </i>
            </InputTextComponent>

            <InputTextComponent
              name="password"
              type="password"
              value={values.password}
              parentInputClass="col-12 my-3"
              inputClass="form-control pr-5"
              placeholder="رمز عبور"
              onChange={this.onChange.bind(this)}
              ref={(node) => this.passwordField = node}
            >
              <i className="fa-lg icon-key position-absolute text-secondary text-muted"
                style={{ top: "7px", right: "32px" }}>
              </i>
            </InputTextComponent>

            <div className="col-12 my-3">
              <input type="submit" className="btn btn-primary form-control" value="ورود" />
            </div>

          </div>
        </form>

        <div className="col-10">

            <VerifyMobileNumberComponent mobileNumber={values.mobileNumber} />
            
            <div className="col-12 text-right">
                <Link to="/forget-password">فراموشی رمز عبور</Link>
            </div>

            <div className="col-12 my-3"></div>

            <div className="col-12 text-right">
                <p>حساب کاربری ندارید؟  <Link to="/register">ثبت نام در لینکرپد</Link></p>
            </div>

        </div>

      </RegisterLayout>
    );
  }
}


const mapDispatchToProps = dispatch => ({
    handleDisplayVerifyMobileNumberComponent: status => {
        dispatch(toogleDisplayVerifyMobileNumberComponent(status))
    }
})

export default connect(null, mapDispatchToProps)(LoginView)
