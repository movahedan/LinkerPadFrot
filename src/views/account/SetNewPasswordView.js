import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { toast } from 'react-toastify'
import RegisterLayout from '../../containers/RegisterLayout'
import InputTextComponent from '../../components/form/InputTextComponent'
import AccountController from '../../controllers/AccountController'
import SetNewPasswordViewModel from '../../view_models/account/SetNewPasswordViewModel'
import SetNewPasswordModel from '../../api_models/account/SetNewPasswordModel'
import setNewPasswordValidation from '../../validations/account/setNewPasswordValidation';

class SetNewPasswordView extends Component {

    constructor(props) {
        super(props)
        this.state = {
            values: new SetNewPasswordViewModel().state,
            options: { redirect: false, success: false }
        }
    }


    componentWillMount() {
        const { state } = this.props.location
        if (!state || !state.mobileNumber || !state.resetPasswordToken) {
            const { options } = this.state
            options.redirect = true
            this.setState({ options })
        } else {
            const { values } = this.state
            values.mobileNumber = state.mobileNumber
            values.resetPasswordToken = state.resetPasswordToken
            this.setState({ values })
        }
    }


    onChange(e) {
        const { name, value } = e.target
        const { values } = this.state
        values[name] = value
        this.setState({ values })
    }


    onSubmit(e) {
        e.preventDefault()

        const { values, options } = this.state

        setNewPasswordValidation(this, values)
            .then(() => {
                const setNewPasswordModel = new SetNewPasswordModel(values)
                return AccountController.resetPassword(setNewPasswordModel)
            })
            .then(() => {
                options.redirect = true
                options.success = true
                this.setState({ options, values })
            })
            .catch(error => toast.error(error.message))
    }


    render() {

        const { values, options } = this.state

        if (options.redirect === true && options.success === true) {
          return (
            <Redirect to={{
                pathname: '/login',
                state: { message: 'رمز عبور با موفقیت تغییر کرد', success: true }
            }} />
          )
        } else if(options.redirect === true && options.success === false ) {
            return (
                <Redirect to='/login' />
            )
        }

        return (
          <RegisterLayout>

                <div className="col-12 my-4">
                  <h2 className="text-center">فراموشی رمز عبور</h2>
                </div>

                <div className="col-10 my-4">
                  <h6 className="text-right">رمز عبور جدید خود را وارد کنید</h6>
                </div>

            <form className="col-10 my-4" onSubmit={this.onSubmit.bind(this)}>
                <div className="row">

                    <InputTextComponent
                        name="newPassword"
                        type="password"
                        value={values.newPassword}
                        parentInputClass="col-12 my-3"
                        inputClass="form-control pr-5"
                        placeholder="رمز عبور جدید"
                        onChange={this.onChange.bind(this)}
                        ref={(node) => this.newPasswordField = node}
                    >
                        <i className="fa-lg icon-key position-absolute text-secondary text-muted"
                            style={{ top: "7px", right: "32px" }}>
                        </i>
                    </InputTextComponent>

                    <InputTextComponent
                        name="newPasswordConfirm"
                        type="password"
                        value={values.newPasswordConfirm}
                        parentInputClass="col-12 my-3"
                        inputClass="form-control pr-5"
                        placeholder="تکرار رمز عبور جدید"
                        onChange={this.onChange.bind(this)}
                        ref={(node) => this.newPasswordConfirmField = node}
                    >
                        <i className="fa-lg icon-key position-absolute text-secondary text-muted"
                            style={{ top: "7px", right: "32px" }}>
                        </i>
                    </InputTextComponent>

                <div className="col-12 my-3">
                    <input type="submit" className="btn btn-primary form-control" value="بروز رسانی رمز عبور" />
                </div>

              </div>
            </form>

          </RegisterLayout>
        )
    }
}


export default SetNewPasswordView
