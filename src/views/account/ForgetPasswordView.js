import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { toast } from 'react-toastify'
import InputTextComponent from '../../components/form/InputTextComponent'
import RegisterLayout from '../../containers/RegisterLayout'
import AccountController from '../../controllers/AccountController'
import ForgetPasswordModel from '../../api_models/account/ForgetPasswordModel'
import ForgetPasswordViewModel from '../../view_models/account/ForgetPasswordViewModel'
import forgetPasswordValidation from '../../validations/account/forgetPasswordValidation';



class ForgetPasswordView extends Component {


    constructor(props) {
        super(props)
        this.state = {
            values: new ForgetPasswordViewModel().state,
            options: { redirect: false }
        }
    }


    onChange(e) {
        const { name, value } = e.target
        const { values } = this.state
        values[name] = value
        this.setState({ values })
    }


    onSubmit(e) {
        e.preventDefault()
        
        const { values, options } = this.state

        forgetPasswordValidation(this, values)
            .then(() => {
                const forgetPasswordModel = new ForgetPasswordModel(values)
                return AccountController.sendConfirmationToken(forgetPasswordModel)
            })
            .then(() => {
                options.redirect = true
                this.setState({ options })
            })
            .catch(error => {
                if(typeof error === "object")
                {
                    toast.error(error.message)
                }
            })
    }


    render() {

        const { values, options } = this.state

        if(options.redirect === true) {
            return (
                <Redirect to={{
                    pathname: '/verification',
                    state: { mobileNumber: values.mobileNumber, action: 'changePassword' }
                }} />
            )
        }

        return (
            <RegisterLayout>
                
                <div className="col-12 my-4">
                    <h2 className="text-center">فراموشی رمز عبور</h2>
                </div>

                <div className="col-10 my-4">
                    <h6 className="text-right">شماره تلفن همراه خود را وارد نمایید. کد تایید به این شماره ارسال خواهد شد</h6>
                </div>

                <form className="col-10 my-4" onSubmit={this.onSubmit.bind(this)} >
                    <div className="row">

                        <InputTextComponent 
                            name="mobileNumber"
                            value={ values.mobileNumber }
                            parentInputClass="col-12 my-3"
                            inputClass="form-control pr-5"
                            placeholder="تلفن همراه (۰۹۱۱۰۰۰۰۰۰۰)" 
                            onChange={ this.onChange.bind(this) }
                            ref={ (node) => this.mobileNumberField = node}
                        >
                            <i className="fa fa-mobile-phone fa-2x  position-absolute text-muted" 
                                style={{ top: "3px", right: "35px" }}>
                            </i>
                        </InputTextComponent>

                        <div className="col-12 my-3">
                            <input type="submit" className="btn btn-primary form-control" value="دریافت کد تایید"  />
                        </div>

                    </div>
                </form>

            </RegisterLayout>
        );
    }
}


export default ForgetPasswordView