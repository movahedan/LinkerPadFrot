import React, { Component } from 'react'
import { toast } from 'react-toastify'
import { connect } from 'react-redux'
import { setPictureName, setCroppedImageUrl ,redirectTo, setEditProjectId } from '../../actions/optionAction'
import ProjectLayout from '../../containers/ProjectLayout'
import TextInputAndLabelComponent from '../../components/form/TextInputAndLabelComponent'
import DatePickerAndLabelComponent from '../../components/form/DatePickerAndLabelComponent'
import ProjectUploadSectionComponent from '../../components/project/ProjectUploadSectionComponent'
import CropImageModalComponent from '../../components/form/CropImageModalComponent'
import ProjectEditViewModel from '../../view_models/project/ProjectEditViewModel'
import ProjectModel from '../../api_models/project/ProjectModel'
import ProjectController from '../../controllers/ProjectController'
import editProjectValidation from '../../validations/project/createProjectValidation'


class ProjectEditView extends Component {


    constructor(props) {
        super(props)
        this.state = {
            values: new ProjectEditViewModel().state
        }

        this.onChange = this.onChange.bind(this)
        this.onChangeDate = this.onChangeDate.bind(this)
    }


    componentWillMount() {
        const { match } = this.props
        this.getProjectById(match.params.id)
    }


    getProjectById = (projectId) => {
        ProjectController.getProjectById(projectId)
            .then(project => {
                let { values } = this.state
                const projectEditViewModel = new ProjectEditViewModel(project)
                values = projectEditViewModel.state
                this.setState({ values }, () => {
                    if(values.projectPicture) {
                        this.props.handleSetCroppedImageUrl(values.projectPicture)
                    }
                })
            })
            .catch(error => {
                switch(error.httpStatus){
                    case 404:
                        toast.error(error.message)
                        this.props.redirectTo(true, '/project')
                        break
                    case 401:
                        this.props.redirectTo(true, '/login', {statusCode:401, message: error.message})
                        break
                    default:
                }
            })
    }


    componentWillReceiveProps(newState) {
        const { values } = this.state
        if(values.projectPicture !== newState.projectPictureName) {
            values.projectPicture = newState.projectPictureName
            this.setState({ values })
        }
    }


    componentWillUnmount() {
        this.props.handleSetProjectPictureName(null) 
        this.props.handleSetCroppedImageUrl(null)
        this.props.handleSetEditProjectId(null)
    }


    onChange = (e) => {
        const { name, value } = e.target
        const { values } = this.state
        values[name] = value
        this.setState({ values })
    }


    onChangeDate = (name, date) => {
        const { values } = this.state
        values[name] = date
        this.setState({ values })
    }


    onSubmit = (e) => {
        e.preventDefault()
        const { values } = this.state
        const projectModel = new ProjectModel(values)
        editProjectValidation(this, values)
            .then(() => {
                return ProjectController.edit(projectModel, values.projectId)
            })
            .then(result => {
                if(result.httpStatus === 200) {
                    toast.success(result.message)
                    this.props.redirectTo(true, '/project')
                }
            })
            .catch(error => { 
                if(error.httpStatus === 401) {
                    this.props.redirectTo(true, '/login', {statusCode:401, message: error.message})
                } else if(typeof error === 'object') {
                    toast.error(error.message) 
                }
            })
    }


    render() {

        const { values } = this.state

        return (
            <ProjectLayout>

                <CropImageModalComponent />

                <div className="row bg-white py-3">

                    <div className="container">
                        
                        <div className="row">
                            <div className="col-12">
                                <h2 className="text-right dir-rtl">ویرایش پروژه</h2>
                            </div>
                        </div>

                        <div className="my-3"></div>

                        <form onSubmit={ this.onSubmit.bind(this) }>
                            <div className="form-row">
                                <div className="col-lg-7 col-12 order-first order-lg-last">
                                    <div className="row">
                                        
                                        <div className="col-12">
                                            <div className="row">
                                                <div className="col-md-7 text-right">
                                                    
                                                    <TextInputAndLabelComponent
                                                        name="name"
                                                        label="عنوان پروژه"
                                                        value={values.name}
                                                        onChange={this.onChange}
                                                        ref={node=> this.nameField = node}
                                                    />

                                                    <div className="py-0 py-md-3 py-lg-1 py-xl-3"></div>

                                                    <TextInputAndLabelComponent
                                                        name="company"
                                                        label="نام شرکت"
                                                        value={values.company}
                                                        onChange={this.onChange}
                                                        ref={node=> this.companyField = node}
                                                    />                                                    

                                                    <div className="py-0 py-md-3 py-lg-1 py-xl-3"></div>

                                                    <TextInputAndLabelComponent
                                                        name="code"
                                                        label="کد پروژه"
                                                        value={values.code}
                                                        onChange={this.onChange}
                                                        ref={node=> this.codeField = node}
                                                    />

                                                </div>

                                                <div className="col-md-5 text-right order-md-last order-first">
                                                    <ProjectUploadSectionComponent projectId={values.projectId} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="py-1"></div>

                                        <div className="col-12 mt-1 text-right">
                                            <TextInputAndLabelComponent
                                                name="address"
                                                label="آدرس"
                                                value={values.address}
                                                onChange={this.onChange}
                                                ref={node=> this.addressField = node}
                                            />
                                        </div>

                                        <div className="col-md-6 text-right">
                                            <TextInputAndLabelComponent
                                                name="city"
                                                label="شهر"
                                                value={values.city}
                                                onChange={this.onChange}
                                                ref={node=> this.cityField = node}
                                            />
                                        </div>

                                        <div className="col-md-6 text-right">
                                            <TextInputAndLabelComponent
                                                name="providence"
                                                label="استان"
                                                value={values.providence}
                                                onChange={this.onChange}
                                                ref={node=> this.providenceField = node}
                                            />
                                        </div>

                                        <div className="col-md-6 text-right">
                                            <DatePickerAndLabelComponent
                                                name="endDate"
                                                label="تاریخ پایان پروژه"
                                                value={values.endDate}
                                                onChange={date => {this.onChangeDate("endDate", date)}}
                                            />
                                        </div>

                                        <div className="col-md-6 text-right">
                                            <DatePickerAndLabelComponent
                                                name="startDate"
                                                label="تاریخ شروع پروژه"
                                                value={values.startDate}
                                                onChange={date => {this.onChangeDate("startDate", date)}}
                                            />
                                        </div>

                                    </div>

                                </div>
                                
                                <div className="col-lg-5 text-center">
                                    <img src="/assets/img/map.png" alt="map-section" className="map-section-image" />
                                    <h3 className="dir-rtl text-center map-section-text">بزودی</h3>
                                </div>
                                
                                <div className="col-12 text-right order-last">
                                    <input type="submit" className="btn btn-success" value="ثبت" />
                                </div>

                            </div>
                        </form>
                        
                    </div>
                </div>
            </ProjectLayout>
        )
    }


}


const mapStateToProps = state => ({
    projectPictureName: state.options.pictureName
})


const mapDispatchToProps = dispatch => ({
    handleSetProjectPictureName: projectPictureName => dispatch(setPictureName(projectPictureName)),
    handleSetCroppedImageUrl: croppedImageUrl => dispatch(setCroppedImageUrl(croppedImageUrl)),
    handleSetEditProjectId: editProjectId => dispatch(setEditProjectId(editProjectId)),
    redirectTo: 
        (redirectStatus, redirectPathName, redirectState) => dispatch(redirectTo(redirectStatus, redirectPathName, redirectState))
})


export default connect(mapStateToProps, mapDispatchToProps)(ProjectEditView)