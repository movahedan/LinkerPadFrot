import React, { Component } from 'react'
import ProjectLayout from '../../containers/ProjectLayout'
import CreateProjectButtonLayoutComponent from '../../components/layout/CreateProjectButtonLayoutComponent'
import ProjectController from '../../controllers/ProjectController'
import { redirectTo } from '../../actions/optionAction'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'
import * as validationMessage from '../../utils/validationMessage'
import ProjectTableComponent from '../../components/project/ProjectTableComponent';



class ProjectListView extends Component {


    constructor(props) {
        super(props)
        this.state = {
            projectList: []
        }
    }


    componentWillMount() { this.getProject() }


    getProject() {
        ProjectController.list()
            .then(projectList => {
                if(projectList) { this.setState({ projectList }) }
            })
            .catch(error => { 
                if(error.httpStatus === 401) {
                    this.props.redirectTo(true, '/login', {statusCode:401, message: validationMessage.error("UnathorizedOnServer")})
                } else {
                    toast.error(error.message) 
                }
            })
    }


    render() {

        const { projectList } = this.state

        return (
            <ProjectLayout>

                <CreateProjectButtonLayoutComponent />

                <div className="my-3"></div>

                <div className="container">
                    <div className="row">
                        <div className="col-12 dir-rtl px-0">
                            <ProjectTableComponent projectList={projectList} />
                        </div>
                    </div>
                </div>
                
            </ProjectLayout>
        )
    }
}


const mapDispatchToProps = dispatch => ({
    redirectTo: 
        (redirectStatus, redirectPathName, redirectState) => dispatch(redirectTo(redirectStatus, redirectPathName, redirectState))
})


export default connect(null, mapDispatchToProps)(ProjectListView)