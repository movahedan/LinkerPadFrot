import React, { createContext, useState, useContext } from "react";
import moment from "moment-jalaali";

moment.loadPersian({
  dialect: "persian-modern",
  usePersianDigits: true
});
const defaultMoment = moment();

export const TimeContext = createContext({});

export const TimeProvider = ({children}) => {
  const [value, setValue] = useState(() => defaultMoment);

  const nextDay = () => setValue(moment(value).add(1, "D"));
  const prevDay = () => setValue(moment(value).add(-1, "D"));
  const toDay = () => setValue(defaultMoment);

  return (
    <TimeContext.Provider value={{ value, setValue, nextDay, prevDay, toDay }}>
      {children}
    </TimeContext.Provider>
  );
};

export default () => useContext(TimeContext);
