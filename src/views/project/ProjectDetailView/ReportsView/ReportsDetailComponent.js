import React, { useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import moment from "moment-jalaali";
import useReport from "stores/report/useReport";
import DatePickerComponent from "components/datepicker";
import ReportDetailHeaderComponent from "./ReportsDetailHeaderComponent";

import {
  Container,
  Row,
  Col,
  Card,
  Button,
  ButtonGroup
} from "react-bootstrap";
import useTimeContext from "../useTimeContext";

moment.loadPersian({
  dialect: "persian-modern",
  usePersianDigits: true
});

export default withRouter(({ projectId, match, history, location }) => {
  const { value: momentValue } = useTimeContext();
  const { reports, dispatch } = useReport();
  const [activeTab, setActiveTab] = useState("data");
  //data, doc, sign

  const [
    { loading: loadingGet, error: errorGet },
    refetch
  ] = dispatch.getReportList();

  let todayReport = reports[projectId]
    ? reports[projectId].filter(
        report =>
          moment(report.createDate)
            .add(1, "d")
            .isAfter(momentValue) &&
          moment(report.createDate)
            .add(-1, "sd")
            .isBefore(momentValue)
      )
    : [];

  const selfReport = location.state
    ? todayReport.filter(
        report => report.reportId === location.state.reportId
      )[0]
    : todayReport.length
    ? todayReport[0]
    : null;
  const otherReports = todayReport.filter(
    report => report.reportId !== selfReport.reportId
  );

  const [tablesExpandState, setTablesExpandState] = useState({});

  useEffect(() => {
    const fn = async () => {
      if (todayReport.length === 0) await refetch();
    };
    fn();
  }, []);

  if (loadingGet) return "لطفا منتظر بمانید";
  if (errorGet) return "مشکلی پیش آمده است";
  if (reports[projectId] === undefined) return null;

  console.log("report detail", tablesExpandState);

  return (
    <>
      <ReportDetailHeaderComponent first={selfReport} other={otherReports} />
      <Container>
        <Row className="my-3">
          <section className="ml-auto mr-3">
            <DatePickerComponent />
          </section>
          <section className="mr-auto ml-3 dir-ltr d-flex align-items-center">
            <ButtonGroup>
              <Button
                onClick={() => setActiveTab("sign")}
                variant={activeTab === "sign" ? "primary" : "light"}
              >
                امضا
              </Button>
              <Button
                onClick={() => setActiveTab("doc")}
                variant={activeTab === "doc" ? "primary" : "light"}
                disabled
              >
                مستندات گزارش
              </Button>
              <Button
                onClick={() => setActiveTab("data")}
                variant={activeTab === "data" ? "primary" : "light"}
              >
                داده های گزارش
              </Button>
            </ButtonGroup>
          </section>
        </Row>

        {activeTab === "data" && selfReport
          ? selfReport.reportTableViewModels.map(table => {
              const open = tablesExpandState[table.reportTableId];
              return (
                <Row className="report-table my-2" key={table.reportTableId}>
                  <Col>
                    <Row
                      className={`table-name px-3 ${open ? "expanded" : ""}`}
                      onClick={() =>
                        setTablesExpandState({
                          ...tablesExpandState,
                          [table.reportTableId]: !tablesExpandState[
                            table.reportTableId
                          ]
                        })
                      }
                    >
                      <h6>{table.name}</h6>
                      <span className="mr-auto">
                        <i className="fas fa-plus-circle vertical-align-middle fa-lg" />
                        {open ? (
                          <i className="fas fa-angle-up vertical-align-middle fa-lg mr-2" />
                        ) : (
                          <i className="fas fa-angle-down vertical-align-middle fa-lg mr-2" />
                        )}
                      </span>
                    </Row>
                    <Row className={open ? "collapse show" : "collapse"} />
                  </Col>
                </Row>
              );
            })
          : null}
      </Container>
    </>
  );
});
