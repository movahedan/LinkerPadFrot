import React, { useState, useEffect, useRef } from "react";
import { withRouter, Switch, Route } from "react-router-dom";
// import moment from "moment-jalaali";

import ReportsListComponent from "./ReportsListComponent";
import ReportDetailComponent from "./ReportsDetailComponent";

const ReportsView = ({ match }) => {
  return (
    <Switch>
      <Route
        exact
        path={`${match.url}`}
        render={() => <ReportsListComponent projectId={match.params.id} />}
      />
      <Route
        path={`${match.url}/reports/detail`}
        render={() => <ReportDetailComponent projectId={match.params.id} />}
      />
    </Switch>
  );
};

export default withRouter(ReportsView);
