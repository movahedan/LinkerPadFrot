import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { Container, Row, Col, Card, Button } from "react-bootstrap";

export default withRouter(({ first, other, history, match }) => {
  const [expand, setExpand] = useState(false);

  useEffect(() => setExpand(false), [first])

  return (
    <Container fluid>
      {first ? (
        <Row
          onClick={() => setExpand(!expand)}
          className={`first-of-reports-wrapper border-bottom-light-grey`}
        >
          <Col>
            <Container>
              <Row
                className={`first-of-reports px-3 ${
                  expand ? "expanded" : ""
                } ${other.length ? "expandable" : ""}`}
              >
                <Col xs={12} md={3}>
                  {first.name}
                </Col>
                <Col xs={12} md={3}>
                  <span className="ml-1">پیمانکار: </span>
                  <span>{first.contractor}</span>
                </Col>
                <Col xs={12} md={3}>
                  <span className="ml-1">نوبت کاری: </span>
                  <span>{first.shift}</span>
                </Col>
                <Col xs={12} md={3}>
                  <span className="ml-1">محل فعالیت: </span>
                  <span>{first.location}</span>
                </Col>
              </Row>
            </Container>
          </Col>
        </Row>
      ) : (
        <Row className="first-of-reports-wrapper border-bottom-light-grey">
          <Col>
            <Container>
              <Row className="first-of-reports px-3">
                <Col>هیچ گزارشی برای امروز ثبت نشده است.</Col>
              </Row>
            </Container>
          </Col>
        </Row>
      )}
      <div className={expand ? "collapse show" : "collapse"}>
        {other.length > 0
          ? other.map(report => {
              return (
                <Row
                  onClick={() =>
                    history.replace({
                      pathname: `${match.url}`,
                      state: { reportId: report.reportId }
                    })
                  }
                  className={
                    "cursor-pointer report-item-wrapper border-bottom-light-grey"
                  }
                  key={report.reportId}
                >
                  <Col>
                    <Container>
                      <Row className={`px-3 report-item`}>
                        <Col xs={12} md={3}>
                          {report.name}
                        </Col>
                        <Col xs={12} md={3}>
                          <span className="ml-1">پیمانکار: </span>
                          <span>{report.contractor}</span>
                        </Col>
                        <Col xs={12} md={3}>
                          <span className="ml-1">نوبت کاری: </span>
                          <span>{report.shift}</span>
                        </Col>
                        <Col xs={12} md={3}>
                          <span className="ml-1">محل فعالیت: </span>
                          <span>{report.location}</span>
                        </Col>
                      </Row>
                    </Container>
                  </Col>
                </Row>
              );
            })
          : null}
      </div>
    </Container>
  );
});
