import React, { useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import moment from "moment-jalaali";
import useReport from "stores/report/useReport";
import DatePickerComponent from "components/datepicker";
import DownloadReportsModal from "./DownloadReportsModal";

import { Container, Row, Col, Card, Button } from "react-bootstrap";
import useTimeContext from "../useTimeContext";

moment.loadPersian({
  dialect: "persian-modern",
  usePersianDigits: true
});

export default withRouter(({ projectId, match, history }) => {
  const { value: momentValue } = useTimeContext();
  const { reports, dispatch } = useReport();

  const [downloadModalState, setDownloadModalState] = useState(false);

  const [
    { loading: loadingGet, error: errorGet },
    refetch
  ] = dispatch.getReportList();

  let projectReports = reports[projectId]
    ? reports[projectId] : [];

  useEffect(() => {
    const fn = async () => {
      if (projectReports.length === 0) await refetch();
    };
    fn();
  }, []);

  if (loadingGet) return "لطفا منتظر بمانید";
  if (errorGet) return "مشکلی پیش آمده است";
  if (reports[projectId] === undefined) return null;

  return (
    <Container>
      <DownloadReportsModal
        open={downloadModalState}
        toggle={() => setDownloadModalState(!downloadModalState)}
        reports={projectReports}
      />
      <Row className="my-3 report-list-header">
        <Button variant={"outline-success"} className="circular m-2">
          <i className="fas fa-plus vertical-align-middle ml-2" />
          گزارش جدید
        </Button>
        <Button
          variant={"outline-warning"}
          className="circular m-2"
          onClick={() => setDownloadModalState(!downloadModalState)}
          disabled={projectReports.length === 0}
        >
          <i className="fa fa-download vertical-align-middle ml-2" />
          دانلود گزارش ها
        </Button>
        <section className="ml-3">
          <DatePickerComponent />
        </section>
      </Row>
      <Row className="mt-4">
        {projectReports.length > 0 ? (
          projectReports.map(report => {
            return (
              <Col
                onClick={() =>
                  history.push({
                    pathname: `${match.url}/reports/detail`,
                    state: { reportId: report.reportId }
                  })
                }
                className={"cursor-pointer"}
                key={report.reportId}
                xs={12}
                md={6}
                lg={4}
              >
                <Card className="circular">
                  <Card.Header className="bg-primary">
                    {report.name}
                  </Card.Header>
                  <Card.Body as={Row} className="m-0">
                    <Col>
                      <Row className="mb-2">
                        <span>
                          <i className="far fa-user-circle fa-lg ml-2" />
                          {report.contractor}
                        </span>
                      </Row>
                      <Row className="mb-2">
                        <span>
                          <i className="far fa-sun fa-lg ml-2" />
                          {report.shift}
                        </span>
                      </Row>
                      <Row className="mb-2">
                        <span>
                          <i className="fas fa-map-marker-alt fa-lg ml-2" />
                          {report.location}
                        </span>
                      </Row>
                    </Col>
                    <Col xs={2} className="px-0 text-left text-primary">
                      <i className="fas fa-file fa-5x" />
                    </Col>
                  </Card.Body>
                </Card>
              </Col>
            );
          })
        ) : (
          <Col>
            <h5 className="text-center">گزارشی برای امروز یافت نشد.</h5>
          </Col>
        )}
      </Row>
    </Container>
  );
});
