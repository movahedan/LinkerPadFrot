import React, { useState } from "react";
import localConfigs from "local-configs";
import { Row, Col, Modal, Card, Form, Button } from "react-bootstrap";

export default ({ open, toggle, reports }) => {
  const [selectedReports, setSelectedReports] = useState(() => []);
  const [selectedFormat, setSelectedFormat] = useState(() => ({
    csv: false,
    pdf: false
  }));

  return (
    <Modal show={open} onHide={toggle}>
      <Modal.Header closeButton>
        <Modal.Title>
          <h5>دانلود گزارش ها</h5>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ minHeight: "30vh" }}>
        <h6>انتخاب گزارش ها</h6>
        <p>میتوانید چند گزارش را با هم انتخاب نمایید.</p>

        <Row className="text-center">
          <Col className="d-flex">
            <span className="small ml-auto px-3 py-2">نام گزارش</span>
            <span className="small mr-auto px-3 py-2">انتخاب</span>
          </Col>
        </Row>
        <Card style={{ height: "20vh", overflow: "auto" }}>
          <Card.Body className="p-3">
            {reports.length > 0 ? (
              reports.map(report => {
                const idx = selectedReports.indexOf(report.reportId);

                return (
                  <Row key={report.reportId} className="text-center">
                    <Col className="d-flex px-2 py-1">
                      <span className="small ml-auto mr-2">{report.name}</span>
                      <span className="small mr-auto ml-2">
                        <Form.Check
                          type={"checkbox"}
                          value={idx > -1}
                          onChange={() => {
                            if (idx > -1)
                              setSelectedReports(
                                selectedReports.filter(
                                  id => id !== selectedReports[idx]
                                )
                              );
                            else
                              setSelectedReports([
                                ...selectedReports,
                                report.reportId
                              ]);
                          }}
                        />
                      </span>
                    </Col>
                    <a
                      target="_blank"
                      href={
                        localConfigs.baseUrl +
                        "/pdf/" +
                        report.reportId +
                        "/" +
                        report.createDate
                      }
                      alt="pdf"
                      className="d-none"
                      id={`pdf-download-${report.reportId}`}
                    />
                    <a
                      target="_blank"
                      href={
                        localConfigs.baseUrl +
                        "/excel/" +
                        report.reportId +
                        "/" +
                        report.createDate
                      }
                      alt="excel"
                      className="d-none"
                      id={`excel-download-${report.reportId}`}
                    />
                  </Row>
                );
              })
            ) : (
              <Row>
                <Col>
                  <h5 className="text-center">
                    هنوز برای این پروژه گزارشی ثبت نشده است.
                  </h5>
                </Col>
              </Row>
            )}
          </Card.Body>
        </Card>

        <h6>فرمت دانلود</h6>
        <Form.Check
          inline
          type={"checkbox"}
          label={<span className="mr-2">CSV</span>}
          value={selectedFormat.csv}
          disabled={selectedReports.length === 0}
          onChange={() =>
            setSelectedFormat({
              csv: !selectedFormat.csv,
              pdf: selectedFormat.pdf
            })
          }
        />
        <Form.Check
          inline
          type={"checkbox"}
          label={<span className="mr-2">PDF</span>}
          value={selectedFormat.pdf}
          disabled={selectedReports.length === 0}
          onChange={() =>
            setSelectedFormat({
              csv: selectedFormat.csv,
              pdf: !selectedFormat.pdf
            })
          }
        />
      </Modal.Body>
      <Modal.Footer className="p-0 mt-3">
        <div className="col px-0 mx-0">
          <Button
            type="button"
            className="btn btn-success h-100 w-100 border-radius-none"
            disabled={
              selectedReports.length === 0 ||
              (!selectedFormat.csv && !selectedFormat.pdf)
            }
            onClick={() => {
              selectedReports.forEach(reportId => {
                selectedFormat.csv &&
                  document.getElementById(`excel-download-${reportId}`).click();
                selectedFormat.pdf &&
                  document.getElementById(`pdf-download-${reportId}`).click();
              });
            }}
          >
            دانلود
          </Button>
        </div>
        <div className="col px-0 mx-0">
          <Button
            type="button"
            className="btn btn-secondary h-100 w-100 border-radius-none text-white"
            data-dismiss="modal"
            onClick={toggle}
          >
            لغو
          </Button>
        </div>
      </Modal.Footer>
    </Modal>
  );
};
