import React, { useState, useEffect, useRef, createContext } from "react";
import { Link, withRouter } from "react-router-dom";

// import { toast } from "react-toastify";

import ReportsView from "./ReportsView";

import ProjectLayout from "containers/ProjectLayout";
import messages from "utils/validationMessage/messages";

import {
  Container,
  Row,
  Col,
  Navbar,
  Nav,
  Image,
  Button,
  Card
} from "react-bootstrap";
import "./style.scss";
import { TimeProvider } from "./useTimeContext";
import { ReportsProvider } from "stores/report/useReport";
import ProjectController from "controllers/ProjectController";
import ProjectEditViewModel from "view_models/project/ProjectEditViewModel";
import projectPictureDefault from "../../../assets/img/projectPictureDefault.svg";
import localConfigs from "local-configs";

const ProjectDetailView = ({ match, history }) => {
  const projectId = match.params.id;
  const [projectTitle, setProjectTitle] = useState("");
  const [projectPictureAddress, setProjectPictureAddress] = useState("");
  const [activeTab, setActiveTab] = useState("reports");

  const getProjectById = projectId => {
    ProjectController.getProjectById(projectId).then(project => {
      const projectEditViewModel = new ProjectEditViewModel(project);
      setProjectPictureAddress(
        projectEditViewModel.state.projectPicture
          ? localConfigs.baseUrl + projectEditViewModel.state.projectPicture
          : projectPictureDefault
      );
      setProjectTitle(projectEditViewModel.state.name);
    });
  };

  useEffect(() => getProjectById(projectId), []);

  return (
    <ProjectLayout>
      <Row className="bg-primary" as={"header"}>
        <Container>
          <Row>
            <Col className="py-2 px-0 dir-rtl">
              <Navbar expand="lg">
                <Navbar.Brand as={Link} to={"/profile"}>
                  <Image
                    src={projectPictureAddress}
                    className="float-right header-logo"
                    alt="linkerpad"
                  />
                </Navbar.Brand>
                <div className="ml-auto mr-3">
                  <div className="h4 text-white text-right">{projectTitle}</div>
                  <Link to={"/project/" + projectId + "/edit"}>
                    <Button variant="outline-light" className="circular">
                      <i className="fas fa-cog vertical-align-middle ml-1" />
                      تنظیمات
                    </Button>
                  </Link>
                </div>
              </Navbar>
            </Col>
          </Row>
        </Container>
      </Row>
      <Row
        className="bg-light"
        as={"header"}
        className="border-bottom-light-grey"
      >
        <Container>
          <Row>
            <Col className="py-0 px-3 dir-rtl">
              <Navbar expand="lg" className="py-0 justify-content-flex-start">
                <Nav.Link
                  onClick={() => {
                    setActiveTab("reports");
                    history.replace(match.url);
                  }}
                  className={`py-3 tab-item ${
                    activeTab === "reports" ? "active" : ""
                  }`}
                >
                  <span
                    className={`h5 text-dark ${
                      activeTab === "reports"
                        ? "font-weight-bold"
                        : "font-weight-normal"
                    }`}
                  >
                    گزارش روزانه
                  </span>
                </Nav.Link>
                <Nav.Link
                  onClick={() => setActiveTab("team")}
                  className={`py-3 tab-item ${
                    activeTab === "team" ? "active" : ""
                  }`}
                >
                  <span
                    className={`h5 text-dark ${
                      activeTab === "team"
                        ? "font-weight-bold"
                        : "font-weight-normal"
                    }`}
                  >
                    تیم
                  </span>
                </Nav.Link>
              </Navbar>
            </Col>
          </Row>
        </Container>
      </Row>

      {activeTab === "reports" ? (
        <Row>
          <ReportsProvider projectId={projectId}>
            <TimeProvider>
              <ReportsView />
            </TimeProvider>
          </ReportsProvider>
        </Row>
      ) : null}
    </ProjectLayout>
  );
};

export default withRouter(ProjectDetailView);
