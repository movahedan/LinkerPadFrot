import React, { Component } from 'react'
import ProjectLayout from '../../containers/ProjectLayout'
import TextInputAndLabelComponent from '../../components/form/TextInputAndLabelComponent'
import ProjectCreateViewModel from '../../view_models/project/ProjectCreateViewModel'
import ProjectUploadSectionComponent from '../../components/project/ProjectUploadSectionComponent'
import DatePickerAndLabelComponent from '../../components/form/DatePickerAndLabelComponent'
import CropImageModalComponent from '../../components/form/CropImageModalComponent'
import { connect } from 'react-redux'
import { setPictureName, setCroppedImageUrl ,redirectTo } from '../../actions/optionAction'
import ProjectModel from '../../api_models/project/ProjectModel'
import ProjectController from '../../controllers/ProjectController'
import { toast } from 'react-toastify'
import * as validationMessage from '../../utils/validationMessage'
import createProjectValidation from '../../validations/project/createProjectValidation'


class ProjectCreateView extends Component {


    constructor(props) {
        super(props)
        this.state = {
            values: new ProjectCreateViewModel().state
        }

        this.onChange = this.onChange.bind(this)
        this.onChangeDate = this.onChangeDate.bind(this)
    }


    componentWillReceiveProps(newState) {
        const { values } = this.state
        if(values.projectPicture !== newState.projectPictureName) {
            values.projectPicture = newState.projectPictureName
            this.setState({ values })
        }
    }


    componentWillUnmount() {
        this.props.handleSetProjectPictureName(null) 
        this.props.handleSetCroppedImageUrl(null)
    }


    onChange = (e) => {
        const { name, value } = e.target
        const { values } = this.state
        values[name] = value
        this.setState({ values })
    }


    onChangeDate = (name, date) => {
        const { values } = this.state
        values[name] = date
        this.setState({ values })
    }


    onSubmit = (e) => {
        e.preventDefault()
        const { values } = this.state
        const projectModel = new ProjectModel(values)
        createProjectValidation(this, values)
            .then(() => {
                return ProjectController.create(projectModel)
            })
            .then(result => {
                if(result.httpStatus === 200) {
                    toast.success(validationMessage.success("ProjectSubmitted"))
                    this.props.redirectTo(true, '/project')
                }
            })
            .catch(error => { 
                if(error.httpStatus === 401) {
                    this.props.redirectTo(true, '/login', {statusCode:401, message: validationMessage.error("UnathorizedOnServer")})
                } else if(typeof error === 'object') {
                    toast.error(error.message) 
                }
            })
    }


    render() {

        const { values } = this.state

        return (
            <ProjectLayout>

                <CropImageModalComponent />

                <div className="row bg-white py-3">

                    <div className="container">
                        
                        <div className="row">
                            <div className="col-12">
                                <h2 className="text-right dir-rtl">ایجاد پروژه</h2>
                            </div>
                        </div>

                        <div className="my-3"></div>

                        <form onSubmit={ this.onSubmit.bind(this) }>
                            <div className="form-row">
                                <div className="col-lg-7 col-12 order-first order-lg-last">
                                    <div className="row">
                                        
                                        <div className="col-12">
                                            <div className="row">
                                                <div className="col-md-7 text-right">
                                                    
                                                    <TextInputAndLabelComponent
                                                        name="name"
                                                        label="عنوان پروژه"
                                                        value={values.name}
                                                        onChange={this.onChange}
                                                        ref={node=> this.nameField = node}
                                                    />

                                                    <div className="py-0 py-md-3 py-lg-1 py-xl-3"></div>

                                                    <TextInputAndLabelComponent
                                                        name="company"
                                                        label="نام شرکت"
                                                        value={values.company}
                                                        onChange={this.onChange}
                                                        ref={node=> this.companyField = node}
                                                    />                                                    

                                                    <div className="py-0 py-md-3 py-lg-1 py-xl-3"></div>

                                                    <TextInputAndLabelComponent
                                                        name="code"
                                                        label="کد پروژه"
                                                        value={values.code}
                                                        onChange={this.onChange}
                                                        ref={node=> this.codeField = node}
                                                    />

                                                </div>

                                                <div className="col-md-5 text-right order-md-last order-first">
                                                    <ProjectUploadSectionComponent />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="py-1"></div>

                                        <div className="col-12 mt-1 text-right">
                                            <TextInputAndLabelComponent
                                                name="address"
                                                label="آدرس"
                                                value={values.address}
                                                onChange={this.onChange}
                                                ref={node=> this.addressField = node}
                                            />
                                        </div>

                                        <div className="col-md-6 text-right">
                                            <TextInputAndLabelComponent
                                                name="city"
                                                label="شهر"
                                                value={values.city}
                                                onChange={this.onChange}
                                                ref={node=> this.cityField = node}
                                            />
                                        </div>

                                        <div className="col-md-6 text-right">
                                            <TextInputAndLabelComponent
                                                name="providence"
                                                label="استان"
                                                value={values.providence}
                                                onChange={this.onChange}
                                                ref={node=> this.providenceField = node}
                                            />
                                        </div>

                                        <div className="col-md-6 text-right">
                                            <DatePickerAndLabelComponent
                                                name="endDate"
                                                label="تاریخ پایان پروژه"
                                                value={values.endDate}
                                                onChange={date => {this.onChangeDate("endDate", date)}}
                                            />
                                        </div>

                                        

                                        <div className="col-md-6 text-right">
                                            <DatePickerAndLabelComponent
                                                name="startDate"
                                                label="تاریخ شروع پروژه"
                                                value={values.startDate}
                                                onChange={date => {this.onChangeDate("startDate", date)}}
                                            />
                                        </div>

                                    </div>

                                </div>
                                
                                <div className="col-lg-5 text-center">
                                    <img src="/assets/img/map.png" alt="map-section" className="map-section-image" />
                                    <h3 className="dir-rtl text-center map-section-text">بزودی</h3>
                                </div>
                                
                                <div className="col-12 text-right order-last">
                                    <input type="submit" className="btn btn-success" value="ثبت" />
                                </div>

                            </div>
                        </form>
                        
                    </div>
                </div>
            </ProjectLayout>
        )
    }


}


const mapStateToProps = state => ({
    projectPictureName: state.options.pictureName
})


const mapDispatchToProps = dispatch => ({
    handleSetProjectPictureName: projectPictureName => dispatch(setPictureName(projectPictureName)),
    handleSetCroppedImageUrl: croppedImageUrl => dispatch(setCroppedImageUrl(croppedImageUrl)),
    redirectTo: 
        (redirectStatus, redirectPathName, redirectState) => dispatch(redirectTo(redirectStatus, redirectPathName, redirectState))
})


export default connect(mapStateToProps, mapDispatchToProps)(ProjectCreateView)