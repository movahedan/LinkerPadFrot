import * as validationMessage from '../../utils/validationMessage'


const createProjectValidation = (fields, values) => {
    return new Promise((resolve, reject) => {

        let validateForm = true

        if (!values.name) {
            const message = validationMessage.error('RequiredName')
            fields.nameField.setMessage('invalid', message)
            validateForm = false
        } else if (values.name.length > 200) {
            const message = validationMessage.error('MaximumLenghtReached')
            fields.nameField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.nameField.setMessage()
        }
    
        
        if (values.code.length > 100) {
            const message = validationMessage.error('CodeMaximumLenghtReached')
            fields.codeField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.codeField.setMessage()
        }
        
        if (!values.company) {
            const message = validationMessage.error('RequiredCompany')
            fields.companyField.setMessage('invalid', message)
            validateForm = false
        } else if (values.company.length > 100) {
            const message = validationMessage.error('CompanyMaximumLenghtReached')
            fields.companyField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.companyField.setMessage()
        }
    
        if (values.address.length > 500) {
            const message = validationMessage.error('MaximumLenghtReached')
            fields.addressField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.addressField.setMessage()
        }

        if (values.providence.length > 50) {
            const message = validationMessage.error('MaximumLenghtReached')
            fields.providenceField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.providenceField.setMessage()
        }

        if (values.city.length > 50) {
            const message = validationMessage.error('MaximumLenghtReached')
            fields.cityField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.cityField.setMessage()
        }

        if (!validateForm) {
            return reject(false)
        }

        return resolve()

    })
}


export default createProjectValidation