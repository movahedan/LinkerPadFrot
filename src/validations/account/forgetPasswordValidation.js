import * as validationMessage from '../../utils/validationMessage'


const forgetPasswordValidation = (fields, values) => {
    return new Promise((resolve, reject) => {
        
        let validateForm = true

        if(!/09[0-9]{9}$/.test(values.mobileNumber)) {
            const message = validationMessage.error('MobileNumberWrongFormat')
            fields.mobileNumberField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.mobileNumberField.setMessage()
        }
        
        if (!validateForm) {
            return reject(false)
        }

        return resolve()

    })
}


export default forgetPasswordValidation