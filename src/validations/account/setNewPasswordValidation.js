import * as validationMessage from '../../utils/validationMessage'


const setNewPasswordValidation = (fields, values) => {
    return new Promise((resolve, reject) => {

        let validateForm = true

        if (values.newPassword.length === 0) {
            const message = validationMessage.error('RequiredPassword')
            fields.newPasswordField.setMessage('invalid', message)
            validateForm = false
        } else if (values.newPassword.length < 6) {
            const message = validationMessage.error('PasswordMinimumLenght')
            fields.newPasswordField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.newPasswordField.setMessage()
        }

        if (values.newPasswordConfirm.length === 0) {
            const message = validationMessage.error('RequiredPassword')
            fields.newPasswordConfirmField.setMessage('invalid', message)
            
            validateForm = false
        } else if (values.newPasswordConfirm.length < 6) {
            const message = validationMessage.error('PasswordMinimumLenght')
            fields.newPasswordConfirmField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.newPasswordConfirmField.setMessage()
        }

        if (values.newPassword !== values.newPasswordConfirm) {
            const message = validationMessage.error('PasswordsAreNotEquals')
            return reject({ status:400, message })
        }
    
        if (!validateForm) {
            return reject(false)
        }

        return resolve()

    })
}


export default setNewPasswordValidation