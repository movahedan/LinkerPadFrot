import * as validationMessage from '../../utils/validationMessage'

const registerValidation = (fields, values) => {
    return new Promise((resolve, reject) => {

        let validateForm = true

        if (!values.firstName) {
            const message = validationMessage.error('RequiredFirstName')
            fields.firstNameField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.firstNameField.setMessage()
        }
    
        if (!values.lastName) {
            const message = validationMessage.error('RequiredLastName')
            fields.lastNameField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.lastNameField.setMessage()
        }
    
        if (!/09[0-9]{9}$/.test(values.mobileNumber)) {
            const message = validationMessage.error('MobileNumberWrongFormat')
            fields.mobileNumberField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.mobileNumberField.setMessage()
        }
    
        if (values.password.length === 0) {
            const message = validationMessage.error('RequiredPassword')
            fields.passwordField.setMessage('invalid', message)
            validateForm = false
        } else if (values.password.length < 6) {
            const message = validationMessage.error('PasswordMinimumLenght')
            fields.passwordField.setMessage('invalid', message)
            validateForm = false
        } else {
            fields.passwordField.setMessage()
        }
    
        if (!validateForm) {
            return reject(false)
        }

        return resolve()
    });
}

export default registerValidation