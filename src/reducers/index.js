import { combineReducers } from 'redux'
import options from './optionReducer'

export default combineReducers({ options })