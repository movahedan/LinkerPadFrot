import { ActionTypes } from '../actions/optionAction'

const options = (state, action) => {
    switch(action.type) {
        
        case ActionTypes.TOGGLE_DISPLAY_RESEND_VERIFICATIONCODE_LINK:
            return {
                ...state,
                displayResendVerificationCodeLink: action.status
            }
        case ActionTypes.TOGGLE_DISPLAY_VERIFY_MOBILENUMBER_COMPONENT:
            return {
                ...state,
                displayVerifyMobileNumberComponent: action.status
            }
            
        case ActionTypes.TOGGLE_DISPLAY_CROP_IMAGE_MODAL_COMPONENT:
            return {
                ...state,
                displayCropImageModalComponent: action.status,
                projectImageFileForModal: action.imageFile || ''
            }

        case ActionTypes.SET_CROPPED_AREA_PIXELS_OF_IMAGE:
            return {
                ...state,
                croppedAreaPixelsOfImage: action.croppedAreaPixelsOfImage
            }

        case ActionTypes.SET_CROPPED_IMAGE_URL:
            return {
                ...state,
                croppedImageUrl: action.croppedImageUrl
            }

        case ActionTypes.SET_PICTURE_NAME:
            return {
                ...state,
                pictureName: action.pictureName
            }

        case ActionTypes.SET_EDIT_PROJECT_ID:
            return {
                ...state,
                editProjectId: action.editProjectId
            }

        case ActionTypes.REDIRECT_TO:
            return {
                ...state,
                redirect: {
                    status: action.status || false,
                    pathName: action.pathName || null,
                    state: action.state || {}
                }
            }

        case ActionTypes.TOGGLE_DISPLAY_FILE_BROWSER_FOR_PROJECT_PICTURE:
            return {
                ...state,
                displayFileBrowserForProjectPicture: action.status
            }            

        default:
            return {
                displayResendVerificationCodeLink: false,
                displayVerifyMobileNumberComponent: false,
                displayCropImageModalComponent: false,
                displayFileBrowserForProjectPicture: false,
                projectImageFileForModal: '',
                croppedAreaPixelsOfImage: null,
                croppedImageUrl: null,
                pictureName: null,
                editProjectId: null,
                redirect: {
                    status: false,
                    pathName: null,
                    state: {}
                }
            }
    }
}

export default options