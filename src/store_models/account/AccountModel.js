export default (props={}) => ({
  firstName: props.firstName ? props.firstName : "",
  lastName: props.lastName ? props.lastName : "",
  company: props.company ? props.company : "",
  emailAddress: props.emailAddress ? props.emailAddress : "",
  mobileNumber: props.mobileNumber ? props.mobileNumber : "",
  profilePictureAddress: props.profilePictureAddress ? props.profilePictureAddress : "",
  profilePictureName: props.profilePictureName ? props.profilePictureName : "",
  skill: props.skill ? props.skill : "",
  userId: props.userId ? props.userId : ""
})