import React, { createContext, useState, useEffect, useContext } from "react";
import useReportController from "controllers/useReportController";

const ReportsContext = createContext({reports: [], dispatch: {}});
const { useGetReportList, useCreateReport } = useReportController;

const getReportList = (projectId, setter) => () => {
  console.log(projectId)
  const [{ data, ...rest }, callable] = useGetReportList(projectId);

  useEffect(() => {
    if (data) setter({ [projectId]: data.responseObject });
  }, [data]);

  return [{ data, ...rest }, callable];
};

export const ReportsProvider = ({ children, projectId }) => {
  const [reports, setReports] = useState({});

  return (
    <ReportsContext.Provider
      value={{
        reports,
        dispatch: {
          getReportList: getReportList(projectId, props => setReports({...reports, ...props})),
          createReport: useCreateReport
        }
      }}
    >
      {children}
    </ReportsContext.Provider>
  );
};

export default () => {
  return useContext(ReportsContext);
};
