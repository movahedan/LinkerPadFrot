import React, { createContext, useState, useEffect, useContext } from "react";
import useAccountController from "controllers/useAccountController";
import AccountModel from "../../store_models/account/AccountModel";
import ProfileModel from "../../api_models/account/ProfileModel";
import ChangePasswordModel from "../../api_models/account/ChangePasswordModel";

const AccountContext = createContext(AccountModel);

export const AccountProvider = ({ children }) => {
  const [account, setAccount] = useState(AccountModel);
  
  const {
    useInformation,
    useEditInformation,
    useGetProfilePicture,
    useChangePassword,
    useEditProfilePicture,
    useRemoveProfilePicture
  } = useAccountController;

  const [
    {
      data: getInformationData,
      loading: getInformationLoading,
      error: getInformationError
    },
    getInformationCallable
  ] = useInformation();

  const [
    {
      // data: editInformationData,
      loading: editInformationLoading,
      error: editInformationError
    },
    editInformationCallable
  ] = useEditInformation();

  const [
    {
      // data: editProfilePictureData,
      loading: editProfilePictureLoading,
      error: editProfilePictureError
    },
    editProfilePictureCallable
  ] = useEditProfilePicture();

  const [
    {
      // data: removeProfilePictureData,
      loading: removeProfilePictureLoading,
      error: removeProfilePictureError
    },
    removeProfilePictureCallable
  ] = useRemoveProfilePicture();

  const [
    {
      data: changePasswordData,
      loading: changePasswordLoading,
      error: changePasswordError
    },
    changePasswordCallable
  ] = useChangePassword();

  useEffect(() => {
    console.log(getInformationData)
    if (getInformationData)
      setAccount({ ...account, ...getInformationData.responseObject });
    else getInformationCallable();
  }, [getInformationData]);

  // useEffect(() => {
  //   // const retrieve = async url => {
  //   //   const res = await getProfilePicture({ url });
  //   //   console.log(res);
  //   // };
  // }, [getInformationData]);

  return (
    <AccountContext.Provider
      value={{
        account,
        dispatch: {
          getInformation: {
            exec: getInformationCallable,
            loading: getInformationLoading,
            error: getInformationError
          },
          editInformation: {
            exec: async information =>
              await editInformationCallable({ data: ProfileModel(information) }),
            loading: editInformationLoading,
            error: editInformationError
          },
          editProfilePicture: {
            exec: async picture => {
              let formData = new FormData();
              formData.append("file", picture);
              await editProfilePictureCallable({ data: formData });
            },
            loading: editProfilePictureLoading,
            error: editProfilePictureError
          },
          changePassword: {
            data: changePasswordData,
            exec: async props =>
              await changePasswordCallable({ data: ChangePasswordModel(props) }),
            loading: changePasswordLoading,
            error: changePasswordError
          },
          removeProfilePicture: {
            exec: removeProfilePictureCallable,
            loading: removeProfilePictureLoading,
            error: removeProfilePictureError
          }
        }
      }}
    >
      {children}
    </AccountContext.Provider>
  );
};

export default () => {
  return useContext(AccountContext);
};
