export const ActionTypes = {
    TOGGLE_DISPLAY_RESEND_VERIFICATIONCODE_LINK: 'TOGGLE_DISPLAY_RESEND_VERIFICATIONCODE_LINK',
    TOGGLE_DISPLAY_VERIFY_MOBILENUMBER_COMPONENT: 'TOGGLE_DISPLAY_VERIFY_MOBILENUMBER_COMPONENT',
    TOGGLE_DISPLAY_CROP_IMAGE_MODAL_COMPONENT: 'TOGGLE_DISPLAY_CROP_IMAGE_MODAL_COMPONENT',
    SET_CROPPED_AREA_PIXELS_OF_IMAGE: 'SET_CROPPED_AREA_PIXELS_OF_IMAGE',
    SET_CROPPED_IMAGE_URL: 'SET_CROPPED_IMAGE_URL',
    SET_PICTURE_NAME: 'SET_PICTURE_NAME',
    SET_EDIT_PROJECT_ID: 'SET_EDIT_PROJECT_ID',
    REDIRECT_TO: 'REDIRECT_TO',
    TOGGLE_DISPLAY_FILE_BROWSER_FOR_PROJECT_PICTURE: "TOGGLE_DISPLAY_FILE_BROWSER_FOR_PROJECT_PICTURE"
}


export const toggleDisplayResendVerificationCodeLink = status => {
    return {
        type: ActionTypes.TOGGLE_DISPLAY_RESEND_VERIFICATIONCODE_LINK,
        status
    }
}


export const toogleDisplayVerifyMobileNumberComponent = status => {
    return {
        type: ActionTypes.TOGGLE_DISPLAY_VERIFY_MOBILENUMBER_COMPONENT,
        status
    }
}


/**
 * toggle display crop image modal component
 * @param {Boolean} status 
 * @param {File} imageFile 
 */
export const toggleDisplayCropImageModalComponent = (status, imageFile=null) => {
    return {
        type: ActionTypes.TOGGLE_DISPLAY_CROP_IMAGE_MODAL_COMPONENT,
        status,
        imageFile
    }
}


/**
 * set cropped area pixels of image
 * @param {Object} croppedAreaPixelsOfImage 
 */
export const setCroppedAreaPixelsOfImage = (croppedAreaPixelsOfImage=null) => {
    return {
        type: ActionTypes.SET_CROPPED_AREA_PIXELS_OF_IMAGE,
        croppedAreaPixelsOfImage
    }
}


/**
 * set cropped image url
 * @param {String} croppedImageUrl 
 */
export const setCroppedImageUrl = croppedImageUrl => {
    return {
        type: ActionTypes.SET_CROPPED_IMAGE_URL,
        croppedImageUrl
    }
}


/**
 * set pictureName after crop image
 * @param {String} pictureName
 */
export const setPictureName = pictureName => {
    return {
        type: ActionTypes.SET_PICTURE_NAME,
        pictureName
    }
}


/**
 * set edit project id in project edit page
 * @param {String} editProjectId
 */
export const setEditProjectId = editProjectId => {
    return {
        type: ActionTypes.SET_EDIT_PROJECT_ID,
        editProjectId
    }
}


/**
 * redirect to specified route with custom state
 * @param {Boolean} status
 * @param {String} pathName
 * @param {Object} state
 */
export const redirectTo = (status=false, pathName=null, state={}) => {
    return {
        type: ActionTypes.REDIRECT_TO,
        status,
        pathName,
        state
    }
}


/**
 * toggle display file browser for project picture
 * use to CropImageModalComponent and ProjectUploadSectionComponent
 * @param {Boolean} status 
 */
export const toggleDisplayFileBrowserForProjectPicture = (status=false) => {
    return {
        type: ActionTypes.TOGGLE_DISPLAY_FILE_BROWSER_FOR_PROJECT_PICTURE,
        status
    }
}