export default props => ({
	projectId: props.projectId ? props.projectId : "",
	name: props.name ? props.name : "",
	contractor: props.contractor ? props.contractor : "",
	location: props.location ? props.location : "",
	shift: props.shift ? props.shift : "",
})