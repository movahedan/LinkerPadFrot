class ProjectModel {


    constructor({ name, code, address, providence, city, company, latitude, longitude, startDate, endDate, projectPicture }) {
        this._name = name
        this._code = code
        this._address = address
        this._providence = providence
        this._city = city
        this._company = company
        this._latitude = latitude
        this._longitude = longitude
        this._startDate = startDate
        this._endDate = endDate
        this._projectPicture = projectPicture
    }


    get name() { return this._name }


    set name(name) { this._name = name }
    

    get code() { return this._code }


    set code(code) { this._code = code }
    

    get address() { return this._address }


    set address(address) { this._address = address }

    
    get providence() { return this._providence }


    set providence(providence) { this._providence = providence }

    
    get city() { return this._city }


    set city(city) { this._city = city }


    get company() { return this._company }


    set company(company) { this._company = company }


    get latitude() { return this._latitude }


    set latitude(latitude) { this._latitude = latitude }


    get longitude() { return this._longitude }


    set longitude(longitude) { this._longitude = longitude }


    get startDate() { 
        if(!this._startDate) { return null }
        const date = new Date(this._startDate)
        const newDate = date.setDate(date.getDate() + 1)
        return new Date(newDate)
    }


    set startDate(startDate) { this._startDate = startDate }
    

    get endDate() { 
        if(!this._endDate) { return null }
        const date = new Date(this._endDate)
        const newDate = date.setDate(date.getDate() + 1)
        return new Date(newDate)
    }


    set endDate(endDate) { this._endDate = endDate }


    get projectPicture() { return this._projectPicture }


    set projectPicture(projectPicture) { this._projectPicture = projectPicture }


}


export default ProjectModel