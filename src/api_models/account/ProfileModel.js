export default ({firstName, lastName, company, skill} = {}) => ({
  Firstname: firstName,
  LastName: lastName,
  Company: company,
  Skill: skill,
})