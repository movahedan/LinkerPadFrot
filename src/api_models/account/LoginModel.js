class LoginModel {
    

    constructor({mobileNumber, password}) {
        this._mobileNumber = mobileNumber
        this._password = password
    }


    get mobileNumber() {
        return '98' + this._mobileNumber.substr(1)
    }


    set mobileNumber(mobileNumber) {
        this._mobileNumber = mobileNumber
    }


    get password() {
        return this._password
    }


    set password(password) {
        this._password = password
    }


}


export default LoginModel