class SetNewPassword {
    

    constructor({ mobileNumber, newPassword, resetPasswordToken }) {
        this._mobileNumber = mobileNumber
        this._newPassword = newPassword
        this._resetPasswordToken = resetPasswordToken
    }


    get mobileNumber() {
        return '98' + this._mobileNumber.substr(1)
    }


    set mobileNumber(mobileNumber) {
        this._mobileNumber = mobileNumber
    }


    get newPassword() {
        return this._newPassword
    }


    set newPassword(newPassword) {
        this._newPassword = newPassword
    }

    
    get resetPasswordToken() {
        return this._resetPasswordToken
    }


    set resetPasswordToken(resetPasswordToken) {
        this._resetPasswordToken = resetPasswordToken
    }


}


export default SetNewPassword