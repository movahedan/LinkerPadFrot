class LoginModel {
    

    constructor({ firstName, lastName, mobileNumber, password}) {
        this._firstName = firstName
        this._lastName = lastName
        this._mobileNumber = mobileNumber
        this._password = password
    }

    
    get firstName() {
        return this._firstName
    }


    set firstName(firstName) {
        this._firstName = firstName
    }

    
    get lastName() {
        return this._lastName
    }


    set lastName(lastName) {
        this._lastName = lastName
    }


    get mobileNumber() {
        return '98' + this._mobileNumber.substr(1)
    }


    set mobileNumber(mobileNumber) {
        this._mobileNumber = mobileNumber
    }


    get password() {
        return this._password
    }


    set password(password) {
        this._password = password
    }


}


export default LoginModel