class LoginModel {
    

    constructor({mobileNumber, token, resetPasswordToken=''}) {
        this._mobileNumber = mobileNumber
        this._token = token
        this._resetPasswordToken = resetPasswordToken
    }


    get mobileNumber() {
        return '98' + this._mobileNumber.substr(1)
    }


    set mobileNumber(mobileNumber) {
        this._mobileNumber = mobileNumber
    }


    get token() {
        return this._token
    }


    set password(token) {
        this._token = token
    }


    get resetPasswordToken() {
        return this._resetPasswordToken
    }


    set resetPasswordToken(resetPasswordToken) {
        this._resetPasswordToken = resetPasswordToken
    }


}


export default LoginModel