import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { HashRouter, Route, Switch } from 'react-router-dom'
import store from './store'
import './App.scss'
import PrivateRoute from './components/account/PrivateRoute'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import RedirectToComponent from './components/layout/RedirectToComponent'

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>

// Account
const Login = React.lazy(() => import('./views/account/LoginView'))
const Register = React.lazy(() => import('./views/account/RegisterView'))
const Verification = React.lazy(() => import('./views/account/VerificationView'))
const ForgetPassword = React.lazy(() => import('./views/account/ForgetPasswordView'))
const SetNewPassword = React.lazy(() => import('./views/account/SetNewPasswordView'))
const ProfileEdit = React.lazy(() => import('./views/account/ProfileEditView/index'))
//Project
const ProjectListView = React.lazy(() => import('./views/project/ProjectListView'))
const ProjectCreateView = React.lazy(() => import('./views/project/ProjectCreateView'))
const ProjectEditView = React.lazy(() => import('./views/project/ProjectEditView'))
const ProjectDetailView = React.lazy(() => import('./views/project/ProjectDetailView'))

class App extends Component {

	render() {
		return (
			<Provider store={ store } >
				
					<ToastContainer
						position="top-right"
						toastClassName="text-right"
						autoClose={5000}
						hideProgressBar={false}
						newestOnTop
						closeOnClick
						rtl
						pauseOnVisibilityChange
						draggable
						pauseOnHover
					/>

					<HashRouter>
						<RedirectToComponent />
						<React.Suspense fallback={loading()}>
							<Switch>
								<Route exact path="/login" name="ورود" component={Login} />
								<Route exact path="/register" name="عضویت" component={Register} />
								<Route exact path="/verification" name="فعال سازی" component={Verification} />
								<Route exact path="/forget-password" name="فراموشی رمز عبور" component={ForgetPassword} />
								<Route exact path="/set-new-password" name="فراموشی رمز عبور" component={SetNewPassword} />
								<PrivateRoute exact path='/profile' name="پروفایل" component={ProfileEdit} />
								<PrivateRoute exact path='/project' name="لیست پروژه‌ها" component={ProjectListView} />
								<PrivateRoute exact path='/project/create' name="لیست پروژه‌ها" component={ProjectCreateView} />
								<PrivateRoute exact path='/project/:id/edit' name="ویرایش پروژه" component={ProjectEditView} />
								<PrivateRoute path='/project/:id/' name="لیست گزارش ها" component={ProjectDetailView} />
							</Switch>
						</React.Suspense>
					</HashRouter>
			</Provider>
    )
  }
}

export default App