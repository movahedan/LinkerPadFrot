import { useState, useEffect } from "react";

const useForm = (initialValues, validate, callback) => {
  const [values, setValues] = useState(() => initialValues);
  const [errors, setErrors] = useState({});

  useEffect(() => {
    const validate_step = async () => setErrors(await validate(values));
    validate_step();
  }, [values])

  const handleSubmit = event => {
    if (event) event.preventDefault();
    console.log(errors)
    if (Object.keys(errors).length === 0) {
      callback(values);
    }
  };

  const handleChange = event => {
    event.persist();

    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  return {
    handleChange,
    handleSubmit,
    values,
    setValues,
    errors
  };
};

export default useForm;
