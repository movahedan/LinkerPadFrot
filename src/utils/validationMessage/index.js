import messages from './messages'


export const error = role => {
    let message = messages.errors[role]
    return message
}


export const success = role => {
    let message = messages.success[role]
    return message
}