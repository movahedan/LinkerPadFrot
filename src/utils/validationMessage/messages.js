const messages = {
    "errors": {
        "RequiredFirstName": 'نام خود را وارد کنید',
        "RequiredLastName": "نام خانوادگی خود را وارد کنید",
        "RequiredMobileNumber": "شماره تلفن همراه خود را وارد کنید",
        "MobileNumberWrongFormat": "این فرمت 09xxxxxxx صحیح است",
        "MobileNumberExist": "با این تلفن همراه قبلا ثبت نام شده است",
        "RequiredPassword": "رمزعبور خود را وارد کنید",
        "PasswordMinimumLenght": "رمز عبور حداقل۶ کاراکتر است",
        "PasswordsAreNotEquals": "رمز‌های عبور بایکدیگر  مطابقت ندارند",
        "WrongMobileNumberOrPassword": "شماره تلفن همراه یا رمزعبور اشتباه است",
        "MobileNumberNotExist": "شما قبلاً با این شماره تلفن همراه ثبت نام نکرده اید",
        "MobileNumberNotConfirm": "شماره موبایل شما تایید نشده است",
        "RequiredToken": "کد ارسال شده به شماره تلفن همراه خود را وارد کنید",
        "WrongToken": "کد وارد شده غلط است",

        "RequiredName": "نام پروژه خود را وارد کنید",
        "RequiredCompany": "نام شرکت خود را وارد کنید",
        "MaximumLenghtReached": "تعداد کاراکترهای انتخابی بیش از سقف مجاز است",
        "CodeMaximumLenghtReached": "تعداد کاراکترهای انتخابی بیش از سقف مجاز است",
        "CompanyMaximumLenghtReached": "تعداد کاراکترهای انتخابی بیش از سقف مجاز است",
        
        "ProjectNotFound": "پروژه مورد نظر یافت نشد",
        "ProjectPictureNotFound" : "تصویر پروژه یافت نشد",
        "ImageFormatNotSuported": "فقط فرمت های jpg، png و jpeg قابل قبول است",

        "RequiredForm": "فیلدهای با حاشیه قرمز ضروری هستند",
        "UnathorizedOnServer" : "شما اجازه دسترسی به این بخش را ندارید!",
        "InternalErrorOnServer" : "خطای سمت سرور لطفا با پشتیبانی تماس بگیرید",
        "MethodNotAllowed" : "شما مجوز اعمال این عمل را ندارید"
    },

    "success": {
        "ProjectSubmitted": "پروژه با موفقیت ثبت شد",
        "ProjectEdited": "پروژه با موفقیت ویرایش شد",
        "ProjectPictureDeletedSuccessfully": "تصویر پروژه با موفقیت حذف شد",

        "ProfilePictureUploadedSuccessfully": "تصویر پروفایل با موفقیت ویرایش شد",
        "ProfilePictureDeleteSuccessfully": "تصویر پروفایل با موفقیت حذف شد",
        "ProfileEditedSuccessfully": "پروفایل با موفقیت ویرایش شد",
        "ProfileChangesRevertedSuccessfully": "تغییرات شما لغو شد"
    }
}

export default messages