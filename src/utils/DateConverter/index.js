import moment from 'moment-jalaali'

moment.loadPersian({dialect: 'persian-modern'})

export const convertToShamsiDateString = (gregorianDate, format='jYYYY/jMM/jDD') => {
    if(gregorianDate) {
        return moment(gregorianDate).format(format)
    }
    return '-'
}

/**
 * convert Date with string type to Shamsi date
 * @param {String} gregorianDateString 
 * @param {String} format
 * @return {Date}
 */
export const convertStringToShamsiDate = (gregorianDateString, format='jYYYY/jMM/jDD') => {
    if(gregorianDateString) {
        const dateString = moment(new Date(gregorianDateString)).format(format)
        return moment(dateString, format)
    }
    return '-'
}