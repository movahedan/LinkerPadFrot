import useController from "./useController";

export const useInformation = () =>
  useController({
    url: `/api/account/GetUserInformation`,
    method: "GET",
    manual: true,
  });

export const useEditInformation = () =>
  useController({
    url: `/api/account/editUserInformation`,
    method: "post",
    manual: true
  });

export const useChangePassword = () =>
  useController({
    url: `/api/account/changePassword`,
    method: "post",
    manual: true
  });

export const useEditProfilePicture = () =>
  useController({
    url: `/api/account/UploadProfilePicture/`,
    method: "post",
    contentType: "multipart/form-data",
    manual: true
  });

export const useRemoveProfilePicture = () =>
  useController({
    url: `/api/account/deleteProfilePicture`,
    method: "post",
    manual: true
  });

export default {
  useInformation,
  useEditInformation,
  useChangePassword,
  useEditProfilePicture,
  useRemoveProfilePicture
}
