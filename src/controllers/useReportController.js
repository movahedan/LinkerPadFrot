import useController from "./useController";
import CreateReportModel from "../api_models/report/CreateReportModel";

export const useGetReportList = projectId =>
  useController({
    url: `/api/report/getReportList${
      projectId ? "?projectId=" + projectId : ""
    }`,
    method: "get",
    manual: true
  });

export const useCreateReport = () =>
  useController({
    url: `/api/report/CreateReport`,
    method: "post",
    manual: true
  });

export default {
  useGetReportList: projectId => useGetReportList(projectId),
  useCreateReport
};
