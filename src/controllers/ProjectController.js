import BaseController from "./BaseController"
import * as validationMessage from '../utils/validationMessage'


class ProjectController extends BaseController {

    /**
     * upload project picture
     * @use edit project 
     * @param {File} projectImage 
     * @param {String} projectId
     */
    uploadProjectPicture(projectImage, projectId=null) {
        return new Promise((resolve, reject) => {
            const url = "api/project/UploadProjectPicture"
            let formData = new FormData()
            
            if(projectId) {
                formData.set('projectId', projectId)
            }

            formData.append('file', projectImage)
            this.sendFormData(url, formData)
                .then(result => resolve(result))
                .catch(error => {
                    let result = {}
                    switch(error.httpStatus) {
                        case 400:
                            result = {
                                httpStatus: error.httpStatus,
                                message: validationMessage.error("ImageFormatNotSuported")
                            }
                            break
                        default:
                            result = {
                                httpStatus: error.httpStatus,
                                message: error.message
                            }
                    }
                    return reject(result)
                })
        })
    }

    /**
     * create new project
     * @param {ProjectModel} projectModel
     * @returns {Promise}
     */
    create(projectModel) {
        return new Promise((resolve, reject) => {
            const url = 'api/project/CreateProject'
            const { name, code, address, providence, city, company, latitude, longitude, startDate, endDate, projectPicture } = projectModel
            this.insertOrUpdate(url, 'POST', { name, code, address, providence, city, company, latitude, longitude, startDate, endDate, projectPicture })
                .then(response => resolve(response))
                .catch(error => {
                    let result = {}
                    switch(error.httpStatus) {
                        case 400: {
                            result = {
                                httpStatus: error.httpStatus,
                                message: validationMessage.error("RequiredForm")
                            }
                            break
                        }
                        default:
                            result = {
                                httpStatus: error.httpStatus,
                                message: error.message
                            }
                    }
                    return reject(result)
                })
        })
    }


    /**
     * get priject list
     * @returns {Promise}
     */
    list() {
        return new Promise((resolve, reject) => {
            const url = 'api/project/GetProjectList'
            this.getData(url)
                .then(response => resolve(response.responseObject))
                .catch(error => {
                    return reject({
                        httpStatus: error.httpStatus,
                        message: error.message
                    })
                })
            })
    }


    /**
     * get project information  by projectId
     * @param {String} projectId 
     */
    getProjectById(projectId) {
        return new Promise((resolve, reject) => {
            const url = `api/project/GetProjectInformation?projectId=${projectId}`
            this.getData(url)
                .then(response => resolve(response.responseObject))
                .catch(error => {
                    let result = {}
                    switch(error.httpStatus) {
                        case 404: {
                            result = {
                                httpStatus: error.httpStatus,
                                message: validationMessage.error("ProjectNotFound")
                            }
                            break
                        }
                        default:
                            result = {
                                httpStatus: error.httpStatus,
                                message: error.message
                            }
                    }
                    return reject(result)
                })

        })
    }


    /**
     * delete project picture by projectId
     * @param {String} projectId 
     */
    deleteProjectPicture(projectId) {
        return new Promise((resolve, reject) => {
            const url = 'api/project/DeleteProjectPicture'
            this.insertOrUpdate(url, 'POST', {projectId})
                .then(() => {
                    resolve({httpStatus: 200, message: validationMessage.success("ProjectPictureDeletedSuccessfully")})
                })
                .catch(error => {
                    let result = {}
                    switch(error.httpStatus) {
                        case 404: {
                            result = {
                                httpStatus: error.httpStatus,
                                message: validationMessage.error("ProjectPictureNotFound")
                            }
                            break
                        }
                        case 405: {
                            result = {
                                httpStatus: error.httpStatus,
                                message: validationMessage.error("MethodNotAllowed")
                            }
                            break
                        }
                        default:
                            result = {
                                httpStatus: error.httpStatus,
                                message: error.message
                            }
                    }
                    return reject(result)
                })
        })
    }


    /**
     * edit project
     * @param {ProjectModel} projectModel
     * @param {String} projectId
     * @returns {Promise}
     */
    edit(projectModel, projectId) {
        return new Promise((resolve, reject) => {
            const url = 'api/project/EditProject'
            const { name, code, address, providence, city, company, latitude, longitude, startDate, endDate } = projectModel
            this.insertOrUpdate(url, 'POST', { projectId, name, code, address, providence, city, company, latitude, longitude, startDate, endDate })
                .then(() => {
                    resolve({httpStatus: 200, message: validationMessage.success("ProjectEdited")})
                })
                .catch(error => {
                    let result = {}
                    switch(error.httpStatus) {
                        case 400: {
                            result = {
                                httpStatus: error.httpStatus,
                                message: validationMessage.error("RequiredForm")
                            }
                            break
                        }
                        case 405: {
                            result = {
                                httpStatus: error.httpStatus,
                                message: validationMessage.error("MethodNotAllowed")
                            }
                            break
                        }
                        default:
                            result = {
                                httpStatus: error.httpStatus,
                                message: error.message
                            }
                    }
                    return reject(result)
                })
        })
    }
    
}


export default new ProjectController()