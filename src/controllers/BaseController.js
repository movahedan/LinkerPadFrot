import Axios from 'axios'
import Configs from '../local-configs'
import * as validationMessage from '../utils/validationMessage'


export default class BaseController {

    constructor() {
        this.host = Configs.baseUrl;
    }

    
    /**
     * Send request to a server and get data from response
     * @param {String} url 
     * @param {String} method 
     * @param {Object} data
     * @param {Boolean} authorization
     * @returns {Promise}
     */
    getData(url, method='GET', data={}, authorization=true) {
        return new Promise((resolve, reject) => {

            let requestData = {
                method,
                url: this.host + '/' + url
            };

            if(method !== "GET") {
                requestData.data = data;
            }

            if(authorization) {
                requestData.headers =  {
                    "Authorization": localStorage.getItem('token')
                };
            }

            Axios(requestData)
                .then(response => {
                    const result = {
                        httpStatus: response.status,
                        ...response.data
                    }
                    resolve(result);
                })
                .catch(error => {
                    const result = this.generateErrorObject(error)
                    reject(result);
                })
        });
    }


    /**
     * send request with form daata content type
     * @param {String} url 
     * @param {FormData} formData 
     * @param {Boolean} authorization 
     */
    sendFormData(url, formData, authorization=true) {
        return new Promise((resolve, reject) => {
            let headers = {}

            if(authorization) {
                headers = { "Authorization": localStorage.getItem('token') }
            }
            
            url = this.host + '/' + url

            Axios.post(url, formData, {headers})
                .then(response => {
                    const result = {
                        httpStatus: response.status,
                        ...response.data
                    }
                    resolve(result);
                })
                .catch(error => {
                    const result = this.generateErrorObject(error)
                    reject(result);
                })
        })
    }


    /**
     * Send request to a server and get data from response
     * @param {String} url 
     * @param {String} method 
     * @param {Object} data
     * @param {Boolean} authorization
     * @returns {Promise}
     */
    insertOrUpdate(url, method='POST', data={}, authorization=true) {
        return new Promise((resolve, reject) => {

            let requestData = {
                method,
                url: this.host + '/' + url
            };

            if(method !== "GET") {
                requestData.data = data;
            }

            if(authorization) {
                requestData.headers =  {
                    "Authorization": localStorage.getItem('token')
                };
            }

            Axios(requestData)
                .then(response => {
                    const result = {
                        httpStatus: response.status,
                        ...response.data
                    }
                    resolve(result);
                })
                .catch(error => {
                    const result = this.generateErrorObject(error)
                    reject(result);
                })
        })
    }
    

    /**
     * generate error object for sent to Controllers
     * @param {Error} error 
     * @return {Object} {httpStatus, message}
     */
    generateErrorObject(error) {
        let result = {}

        if(!error.response) {
            result = {
                httpStatus: 500,
                message: validationMessage.error("InternalErrorOnServer")
            }
            return result
        }

        switch(error.response.status) {
            case 401:
                result = {
                    httpStatus: 401,
                    message: validationMessage.error("UnathorizedOnServer")
                }
                break

            default: 
                result = {
                    ...error.response,
                    httpStatus: error.response.status,
                }
        }

        return result
    }

}