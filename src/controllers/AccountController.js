import BaseController from './BaseController';
import * as validationMessage from '../utils/validationMessage'


class AccountController extends BaseController {


    /**
     * Login
     * @param {LoginModel} loginModel
     * @returns {Promise}
     */
    login(loginModel) {
        const { mobileNumber, password }  = loginModel
        return new Promise((resolve, reject) => {
            this.getData(`api/account/login`, 'POST', { mobileNumber, password })
                .then(result => {
                    return resolve(result)
                })
                .catch(error => {
                    switch(error.httpStatus) {
                        case 401:
                            error.message = validationMessage.error('WrongMobileNumberOrPassword')
                            break
                        case 405:
                            error.message = validationMessage.error('MobileNumberNotConfirm')
                            break
                        default:
                    }
                    return reject(error)
                })
            ;
        });
    }


    /**
     * Register Api
     * @param {RegisterModel} registerModel
     * @returns {Promise}
     */
    register(registerModel) {
        const { firstName, lastName, mobileNumber, password } = registerModel
        return new Promise((resolve, reject) => {
            this.getData(`api/account/register`, 'POST', { firstName, lastName, mobileNumber, password })
                .then(result => resolve(result))
                .catch(error => {
                    switch(error.httpStatus) {
                        case 409: 
                            error.message = validationMessage.error('MobileNumberExist')
                            break
                        default:
                    }
                    return reject(error)
                })
            ;
        });
    }


    /** 
     * Send verificationCode by SMS
     * @param {ForgetPasswordModel} forgetPasswordModel
     * @returns {Promise}
    */
    sendConfirmationToken(forgetPasswordModel) {
        const { mobileNumber } = forgetPasswordModel
        return new Promise((resolve, reject) => {
            this.getData('api/account/SendConfirmationToken', 'POST', { mobileNumber })
                .then(result => resolve(result))
                .catch(error => {
                    switch(error.httpStatus) {
                        case 404:
                            error.message = validationMessage.error('MobileNumberNotExist')
                            break
                        case 405:
                            error.message = validationMessage.error('MobileNumberNotExist')
                            break
                        default:
                    }
                    return reject(error)
                })
            ;
        });
    }


    /** 
     * Confirmation verificationCode
     * @param {VerificationModel} verificationModel
     * @returns {Promise}
    */
    confirmToken(verificationModel) {
        const { mobileNumber, token } = verificationModel
        return new Promise((resolve, reject) => {
            this.getData('api/account/ConfirmToken', 'POST', { mobileNumber, token })
                .then(result => resolve(result))
                .catch(error => reject(error))
            ;
        });
    }


    /** 
     * Get resetPasswordToken
     * @param {VerificationModel} verificationModel
     * @returns {Promise} resetPasswordToken
    */
    resetPasswordToken(verificationModel) {
        const { mobileNumber, token } = verificationModel
        return new Promise((resolve, reject) => {
            this.getData('api/account/CreateResetPasswordToken', 'POST', { mobileNumber, token })
                .then(result => {
                    const { resetPasswordToken } = result.responseObject;
                    return resolve(resetPasswordToken);
                })
                .catch(error => {
                    switch(error.httpStatus) {
                        case 400:
                            error.message = validationMessage.error('WrongToken')
                            break
                        default:
                    }
                    reject(error)
                })
            ;
        });
    }


    /** 
     * Reset password
     * @param {SetNewPasswordModel}
     * @returns {Promise}
    */
    resetPassword(setNewPasswordModel) {
        const { mobileNumber, newPassword, resetPasswordToken } = setNewPasswordModel
        return new Promise((resolve, reject) => {
            this.getData('api/account/ResetPassword', 'POST', { mobileNumber, newPassword, resetPasswordToken })
                .then(result => resolve(result))
                .catch(error => reject(error))
            ;
        });
    }

    
}


export default new AccountController();