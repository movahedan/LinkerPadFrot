import BaseController from "./BaseController"
import * as validationMessage from '../utils/validationMessage'


class GalleryController extends BaseController {


    /**
     * upload picture and get temporary picture name from server
     * @param {File} imageFile
     * @returns {Promise}
     */
    uploadPicture(imageFile) {
        return new Promise((resolve, reject) => {
            const url = 'api/gallery/uploadPicture'
            let formData = new FormData()
            
            formData.append('file', imageFile)
            this.sendFormData(url, formData)
                .then(result => resolve(result))
                .catch(error => {
                    let result = {}
                    switch(error.httpStatus) {
                        case 400:
                            result = {
                                httpStatus: error.httpStatus,
                                message: validationMessage.error("ImageFormatNotSuported")
                            }
                            break
                        default:
                            result = {
                                httpStatus: error.httpStatus,
                                message: error.message
                            }
                    }
                    return reject(result)
                })
        })
    }


}


export default new GalleryController()