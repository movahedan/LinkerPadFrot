import React, { useEffect } from "react";
import Axios from "axios";
import LRU from "lru-cache";
import configs from "../local-configs";
import { configure } from "axios-hooks";
import useAxios from "axios-hooks";

const axios = Axios.create({
  baseURL: configs.baseUrl,
  headers: {
    "Authorization": localStorage.getItem("token"),
  }
});

const cache = new LRU({ max: 10 });

configure({ axios, cache });

export default ({
  url,
  method,
  data,
  contentType = "application/json",
  manual
}) => {
  const [{ data: responseData, loading, error }, callable] = useAxios(
    {
      url,
      method,
      data,
      headers: {
        "Authorization": localStorage.getItem("token"),
        "Content-type": contentType,
        "Access-Control-Allow-Origin": "*"
      }
    },
    { manual: true }
  );

  useEffect(() => {
    if(error)
      console.log(error)
  }, [error])

  return [{ data: responseData, loading, error }, callable]
}