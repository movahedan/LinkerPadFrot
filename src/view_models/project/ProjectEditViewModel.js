import { convertStringToShamsiDate } from '../../utils/DateConverter'


class ProjectEditViewModel {
    
    
    constructor(props={}) {
      
        this.state = {
            projectId: props.projectId? props.projectId: '',
            name: props.name? props.name: '',
            code: props.code? props.code: '',
            address: props.address? props.address: '',
            providence: props.providence? props.providence: '',
            city: props.city? props.city: '',
            company: props.company? props.company: '',
            latitude: props.latitude? props.latitude: 35.6970114,
            longitude: props.longitude? props.longitude : 51.2093899,
            startDate: props.startDate? convertStringToShamsiDate(props.startDate): null,
            endDate: props.endDate? convertStringToShamsiDate(props.endDate): null,
            projectPicture: props.projectPictureAddress? props.projectPictureAddress: ''
        }

    }

    
}

export default ProjectEditViewModel