

class ProjectCreateViewModel {
    constructor(props) {

        this.state = {
            name: '',
            code: '',
            address: '',
            providence: '',
            city: '',
            company: '',
            latitude: 35.6970114,
            longitude: 51.2093899,
            startDate: null,
            endDate: null,
            projectPicture: ''
        }
    }
}

export default ProjectCreateViewModel