import React, { Component } from 'react'
import { toast } from 'react-toastify'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { redirectTo } from '../../actions/optionAction'



class RedirectToComponent extends Component {


    componentDidMount() { this.handleSetDefaultValueOfRefreshAction(this.props) }


    componentWillReceiveProps(newProps) { this.handleSetDefaultValueOfRefreshAction(newProps) }


    handleSetDefaultValueOfRefreshAction = (props) => {
        const { redirectStatus, redirectState } = props
        if(redirectState.statusCode && redirectState.statusCode === 401) {
            toast.error(redirectState.message)
        }
        
        if(redirectStatus) {
            props.setDefaultValueOfRefreshAction(false)
        }
    }


    render() {
        
        const { redirectStatus, redirectPathName, redirectState } = this.props
        
        if(redirectStatus) {
            return (
                <Redirect to={{
                    pathname:redirectPathName,
                    state: redirectState
                }} />
            )
        } else {
            return ''
        }
         
    }

}


const mapStateToProps = state => ({
    redirectStatus: state.options.redirect.status,
    redirectState: state.options.redirect.state,
    redirectPathName: state.options.redirect.pathName
})


const mapDispatchToProps = dispatch => ({
    setDefaultValueOfRefreshAction: 
        (redirectStatus, redirectPathName, redirectState) => dispatch(redirectTo(redirectStatus, redirectPathName, redirectState))
})


export default connect(mapStateToProps, mapDispatchToProps)(RedirectToComponent)