import React from "react";
import { Link } from "react-router-dom";

import localConfigs from "local-configs";
import useAccount from "stores/account/useAccount";

import {
  Nav,
  Navbar,
  NavDropdown,
  Image,
  Row,
  Container,
  Col
} from "react-bootstrap";
import PNG_user from "assets/img/user-logo.svg";
import "./style.scss";

export default ({ location }) => {
  const { account, dispatch } = useAccount();

  return (
    <Row className="bg-light" as={"header"}>
      <Container>
        <Row>
          <Col className="py-2 px-0 dir-rtl">
            <Navbar expand="lg">
              <Navbar.Brand as={Link} to={"/profile"}>
                <Image
                  src={"/assets/img/logo.png"}
                  className="float-right header-logo"
                  alt="linkerpad"
                />
              </Navbar.Brand>
              <Nav.Link as={Link} to={"/project"}>
                <span
                  className={`
                  h4 ${
                    location === "#/project" ? "text-navy-blue" : "text-dark"
                  }
                `}
                >
                  پروژه‌ها
                </span>
              </Nav.Link>

              <NavDropdown
                title={
                  <Image
                    src={
                      account.profilePictureAddress
                        ? localConfigs.baseUrl + account.profilePictureAddress
                        : PNG_user
                    }
                    className="float-right header-logo"
                    roundedCircle
                    alt="linkerpad"
                  />
                }
                className="mr-auto header-avatar"
                as={Nav.Link}
              >
                <div className="p-1 text-right">
                  <NavDropdown.Item as={Link} to={"/profile"}>
                    حساب کاربری
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to={"/help"}>
                    راهنما
                  </NavDropdown.Item>
                  <NavDropdown.Item as={Link} to={"/logout"}>
                    خروج
                  </NavDropdown.Item>
                </div>
              </NavDropdown>
            </Navbar>
          </Col>
        </Row>
      </Container>
    </Row>
  );
};
