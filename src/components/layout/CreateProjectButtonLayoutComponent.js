import React, { Component } from 'react'
import { Link } from 'react-router-dom'


class CreateProjectButtonLayoutComponent extends Component {


    render() {
        return (
            <div className="row bg-white">
                <div className="container text-right">
                    <div className="row">
                        <div className="col-12 py-2">
                            <Link to="/project/create" className="btn btn-pill btn-outline-success dir-rtl" >+ پروژه جدید</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}


export default CreateProjectButtonLayoutComponent