import React, { Component } from 'react'
import { toggleDisplayResendVerificationCodeLink } from '../../actions/optionAction'
import { connect } from'react-redux'



class CountDownTimerComponent extends Component {


    constructor(props) {
        super(props)
        this.state = {
            values: {
                min: 0,
                sec: 0,
            }
        }

        this.start = this.start.bind(this)
    }


    componentDidMount() { this.start() }


    componentWillUnmount() { this.stop() }


    start() {
        let time = this.props.date
        if(!time) {
            time = new Date().setMinutes(new Date().getMinutes()+2)
        }

        this.interval = setInterval(() => {
          const values = this.calculateCountdown(time)
          if(values) {
              this.setState({ values }) 
          } else {
            this.stop()
            this.props.handleTimeEnd(true)
          }
        }, 1000)
    }


    stop() { clearInterval(this.interval) }

    
    calculateCountdown(endDate) {
        let diff = (Date.parse(new Date(endDate)) - Date.parse(new Date())) / 1000
    
        if (diff <= 0) return false
    
        const timeLeft = {
          min: 0,
          sec: 0,
          millisec: 0,
        }
    
        if (diff >= 60) {
          timeLeft.min = Math.floor(diff / 60)
          diff -= timeLeft.min * 60
        }
        timeLeft.sec = diff
    
        return timeLeft
    }


    addLeadingZeros(value) {
        value = String(value)
        while (value.length < 2) {
          value = '0' + value
        }
        return value
    }


    render() {

        const { values } = this.state
        const { displayResendVerificationCodeLink } = this.props
    
        return (
            <span hidden={ displayResendVerificationCodeLink }>
                <strong>{this.addLeadingZeros(values.min)}</strong>
                :
                <strong>{this.addLeadingZeros(values.sec)}</strong>                
            </span>
        )

    }
}


const mapStateToProps = state => ({
    displayResendVerificationCodeLink: state.options.displayResendVerificationCodeLink
})


const mapDispatchToProps = dispatch => ({
    handleTimeEnd: status => dispatch(toggleDisplayResendVerificationCodeLink(status))
})


export default connect(mapStateToProps, mapDispatchToProps, null, {forwardRef: true})(CountDownTimerComponent)