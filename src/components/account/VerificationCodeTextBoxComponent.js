import React, { Component } from 'react'
import { toast } from 'react-toastify';



class VerificationCodeTextBoxComponent extends Component {


    constructor(props) {
        super(props)
        this.state = {
            values: {
                Token: this.props.Token
            },
            numbers: {
                n1: '',
                n2: '',
                n3: '',
                n4: '',
                n5: ''
            }
        }
        this.handleChange = this.handleChange.bind(this)
        this.checkValiationNumbers = this.checkValiationNumbers.bind(this)
    }


    handleChange(e) {
        const regex = /^[0-9\b]+$/
        const { id, value, tabIndex } = e.target
        if (value === '' || regex.test(value)) {
            const { numbers } = this.state
            numbers[id] = value;
            
            this.setState({ numbers }, () => {
                let token = '';
                for(let key in numbers) {
                    token = token.concat(numbers[key])
                }
                this.props.handleUpdateToken(token)
            })

            if (value !== '' && this.refs[`n${tabIndex+1}`]) {
                this.refs[`n${tabIndex+1}`].focus()
            }
        }
    }


    checkValiationNumbers() {
        const { numbers } = this.state
        let formValidate = true

        if(!numbers.n1) {
            this.refs.n1.className = "w-75 text-verification border-bottom-danger"
            formValidate = false
        } else {
            this.refs.n1.className = "w-75 text-verification border-bottom-default"
        }

        if(!numbers.n2) {
            this.refs.n2.className = "w-75 text-verification border-bottom-danger"
            formValidate = false
        } else {
            this.refs.n2.className = "w-75 text-verification border-bottom-default"
        }

        if(!numbers.n3) {
            this.refs.n3.className = "w-75 text-verification border-bottom-danger"
            formValidate = false
        } else {
            this.refs.n3.className = "w-75 text-verification border-bottom-default"
        }

        if(!numbers.n4) {
            this.refs.n4.className = "w-75 text-verification border-bottom-danger"
            formValidate = false
        } else {
            this.refs.n4.className = "w-75 text-verification border-bottom-default"
        }

        if(!numbers.n5) {
            this.refs.n5.className = "w-75 text-verification border-bottom-danger"
            formValidate = false
        } else {
            this.refs.n5.className = "w-75 text-verification border-bottom-default"
        }

        if(!formValidate) {
            toast.error('لطفا کد ۵ رقمی را وارد کنید')
        }
        
        return formValidate
    }


    render() {

        const { numbers } = this.state

        return (
            <div className="form-row justify-content-between dir-ltr">

                <div className="form-group col-2">
                    <input 
                        tabIndex="1" 
                        id="n1" 
                        ref="n1" 
                        value={numbers.n1} 
                        className="w-75 text-verification border-bottom-default"
                        maxLength="1"
                        onChange={this.handleChange} 
                        autoFocus
                    />
                </div>

                <div className="form-group col-2">
                    <input 
                        tabIndex="2" 
                        id="n2" 
                        ref="n2" 
                        value={numbers.n2} 
                        className="w-75 text-verification border-bottom-default"
                        maxLength="1"
                        onChange={this.handleChange} 
                    />
                </div>

                <div className="form-group col-2">
                    <input 
                        tabIndex="3" 
                        id="n3" 
                        ref="n3" 
                        value={numbers.n3} 
                        className="w-75 text-verification border-bottom-default" 
                        maxLength="1"
                        onChange={this.handleChange} 
                    />
                </div>

                <div className="form-group col-2">
                    <input 
                        tabIndex="4" 
                        id="n4" 
                        ref="n4" 
                        value={numbers.n4} 
                        className="w-75 text-verification border-bottom-default" 
                        maxLength="1"
                        onChange={this.handleChange} 
                    />
                </div>

                <div className="form-group col-2">
                    <input 
                        tabIndex="5" 
                        id="n5" 
                        ref="n5" 
                        value={numbers.n5}
                        className="w-75 text-verification border-bottom-default" 
                        maxLength="1"
                        onChange={this.handleChange} 
                    />
                </div>
                
            </div>
        )
    }
}


export default VerificationCodeTextBoxComponent;