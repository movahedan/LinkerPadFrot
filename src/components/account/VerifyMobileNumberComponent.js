import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import AccountControlelr from '../../controllers/AccountController'
import ForgetPasswordModel from '../../api_models/account/ForgetPasswordModel'


class VerifyMobileNumberComponent extends Component {


    constructor(props) {
        super(props)
        this.state = {
            mobileNumber: this.props.mobileNumber,
            options: { redirect: false }
        }
    }


    componentWillUpdate(nextProps) {
        if(nextProps.mobileNumber !== this.state.mobileNumber)
        {
            const { mobileNumber } = nextProps
            this.setState({ mobileNumber })
        }
    }


    handleVerifyMobileNumber() {
        const { options, mobileNumber } = this.state
        const forgetPasswordModel = new ForgetPasswordModel({ mobileNumber })
        AccountControlelr.sendConfirmationToken(forgetPasswordModel)
            .then(() => {
                options.redirect = true
                this.setState({ options })
            })
            .catch(error => {
                console.log("error", error)
            })
    }


    render() {

        const { displayVerifyMobileNumberComponent } = this.props
        const { options, mobileNumber } = this.state

        if(options.redirect) {
            return (
                <Redirect to={{
                    pathname: '/verification',
                    state: { mobileNumber, action: 'confirmMobileNumber'}
                }} />
            )
        }

        return (
            <div className="col-12 my-4" hidden={!displayVerifyMobileNumberComponent}>
                <span className="mx-2">برای ورود به لینکرپد ابتدا شماره موبایل خود را تایید کنید</span>
                <button 
                    className="btn btn-sm btn-primary mx-2"
                    onClick={ this.handleVerifyMobileNumber.bind(this) } >
                        فعال سازی
                </button>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    displayVerifyMobileNumberComponent: state.options.displayVerifyMobileNumberComponent
})


export default connect(mapStateToProps)(VerifyMobileNumberComponent)