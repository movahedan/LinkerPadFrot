import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'

import { AccountProvider } from 'stores/account/useAccount';
import { ReportsProvider } from 'stores/report/useReport';

export default class PrivateRoute extends Component {


    constructor(props) {
        super(props); 
        this.state = {
            authorization: null
        }

        this.handleCheckToken = this.handleCheckToken.bind(this)
    }


    componentWillMount() {
        this.handleCheckToken()
    }

    
    componentWillReceiveProps(newProps){
        this.handleCheckToken()
    }


    handleCheckToken() {
        const { authorization } = this.props
        const token = localStorage.getItem('token')
        if(authorization !== token) {
            this.setState({ authorization: token })
        }        
    }


    render() {   

        const { authorization } = this.state
        const { component: Component, ...rest } = this.props

        return (
            <Route {...rest} 
                render={ props =>
                    authorization ? (
                        <AccountProvider>
                            <Component {...props} />
                        </AccountProvider>
                    ) : (
                        <Redirect to='/login' />
                    )
                }
            />
        )

    }

} 


