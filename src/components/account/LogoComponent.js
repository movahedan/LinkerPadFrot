import React, { Component } from 'react';



class LogoComponent extends Component {
    render() {
        return (
            <div className="col-12 text-center">
                <img src="/assets/img/logo.png" alt="linkerpad" className="w-25"/>
                <h4 className="text-center dir-rtl font-vazir">لینکِرپَد</h4>
            </div>
        )
    }
}


export default LogoComponent;