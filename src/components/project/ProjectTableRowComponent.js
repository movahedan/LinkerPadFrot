import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { convertToShamsiDateString } from '../../utils/DateConverter'
import projectPictureDefault from '../../assets/img/projectPictureDefault.svg'
import localConfigs from 'local-configs';


class ProjectTableRowComponent extends Component {


    render() {
        
        const { projectList, history } = this.props

        const projectListItem = projectList.map(item => {

            let role = ''
            if(item.userRole === 1 && item.isCreator) {
                role = 'مدیر*'
            } else if (item.userRole === 1 && !item.isCreator) {
                role = 'مدیر'
            } else if (item.userRole === 2) {
                role = 'کارشناس'
            }

            let editProjectLink = null
            if(item.isCreator) {
                editProjectLink = (
                    <Link to={`/project/${item.projectId}/edit`}>
                        <i className=" fa fa-cog fa-2x text-dark"></i>
                    </Link>
                )
            } else {
                editProjectLink = (<i className=" fa fa-cog fa-2x text-muted"></i>)
            }

            return (
                <tr key={item.projectId} onClick={() => history.push(`/project/${item.projectId}`)}>
                    <td className="dir-rtl text-center bg-white">
                        <img 
                            src={item.projectPictureAddress? localConfigs.baseUrl + item.projectPictureAddress : projectPictureDefault} 
                            alt={item.name} 
                            className="project-picture" 
                        />
                    </td>
                    <td className="dir-rtl text-right" >{item.name}</td>
                    <td className="dir-rtl text-center" >{item.projectMemebmerCount}</td>
                    <td className="dir-rtl text-center" >{role}</td>
                    <td className="dir-rtl text-center" >{convertToShamsiDateString(item.startDate)}</td>
                    <td className="dir-rtl text-center" >{item.code? item.code : '-'}</td>
                    <td className="dir-rtl text-center" >
                        {editProjectLink}
                    </td>
                </tr>
            )
        })

        return projectListItem
    }

}


export default withRouter(ProjectTableRowComponent)