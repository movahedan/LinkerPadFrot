import React, { Component } from 'react'
import { toast } from 'react-toastify'
import { connect } from 'react-redux'
import { toggleDisplayCropImageModalComponent, setCroppedImageUrl, setPictureName, setEditProjectId, redirectTo, toggleDisplayFileBrowserForProjectPicture } from '../../actions/optionAction'
import ProjectController from '../../controllers/ProjectController'

class ProjectUploadSectionComponent extends Component {


    componentDidMount() { this.handleShowProjectPicture(this.props) }


    componentWillReceiveProps(newProps) { 
        this.handleShowProjectPicture(newProps)
        
        if(newProps.displayFileBrowserForProjectPicture) {
            this.handleOpenFileBrowser()
        }
    }


    handleShowProjectPicture = ({ croppedImageUrl }) => {
        let projectPictureClassName = "w-100 ",
            closeImageClassName = "fa fa-times-circle fa-2x text-secondary remove-image-icon "

        if(croppedImageUrl) {
            projectPictureClassName += "image-upload-section"
            this.refs.projectPicture.style.backgroundImage = `url(${croppedImageUrl})`
            this.refs.projectPicture.innerHTML = ''
        } else {
            this.refs.projectPicture.style.backgroundImage = 'none'
            projectPictureClassName += "upload-section"
            closeImageClassName += "invisible"
        }

        this.refs.projectPicture.className = projectPictureClassName
        this.refs.closeImageField.className = closeImageClassName
    }


    handleOpenFileBrowser = () => { 
        if(!this.props.croppedImageUrl) {
            this.refs.projectPictureField.click()
        }
        this.props.handleDisplayFileBrowserForProjectPicture(false)
    }


    onChange = (e) => {
        const projectPicture = e.target.files[0]
        if(this.props.projectId) {
            this.props.handleSetEditProjectId(this.props.projectId)
        }
        this.props.handleDisplayCropImageModal(true, projectPicture)
    }


    onRemoveProjectPicture = () => {
        if(this.props.projectId) {
            ProjectController.deleteProjectPicture(this.props.projectId)
                .then((result) => { toast.success(result.message) })
                .catch(error => {
                    switch(error.httpStatus){
                        case 404:
                        case 405:
                            toast.error(error.message)
                            break
                        case 401:
                            this.props.redirectTo(true, '/login', { statusCode:401, message: error.message })
                            break
                        default:
                    }
                })
        }
        this.props.handleSetPictureName(null)
        this.props.handleSetCroppedImageUrl(null)
    }


    render() {

        return (
            <div className="row h-100">
                <div className="col align-self-center">
                    <label className="dir-rtl">عکس پروژه:</label>
                    <span ref="closeImageField" onClick={this.onRemoveProjectPicture.bind(this)}></span>
                    <div ref="projectPicture" onClick={this.handleOpenFileBrowser.bind(this)}></div>
                    <input 
                        id="projectPicture" 
                        name="projectPicture" 
                        type="file" 
                        accept="image/png, image/jpeg"
                        multiple={false}
                        className="d-none" 
                        ref="projectPictureField"
                        onChange={this.onChange.bind(this)}
                    />
                </div>
            </div>
        )
    }


}


const mapStateToProps = state => ({
    croppedImageUrl: state.options.croppedImageUrl,
    displayFileBrowserForProjectPicture: state.options.displayFileBrowserForProjectPicture
})


const mapDispatchToProps = dispatch => ({
    handleDisplayCropImageModal: 
        (status, imageFile) => dispatch(toggleDisplayCropImageModalComponent(status, imageFile)),
    handleSetCroppedImageUrl: croppedImageUrl => dispatch(setCroppedImageUrl(croppedImageUrl)),
    handleSetPictureName: pictureName => dispatch(setPictureName(pictureName)),
    handleSetEditProjectId: editProjectId => dispatch(setEditProjectId(editProjectId)),
    handleDisplayFileBrowserForProjectPicture: status => dispatch(toggleDisplayFileBrowserForProjectPicture(status)),
    redirectTo: 
        (redirectStatus, redirectPathName, redirectState) => dispatch(redirectTo(redirectStatus, redirectPathName, redirectState))
})


export default connect(mapStateToProps, mapDispatchToProps)(ProjectUploadSectionComponent)