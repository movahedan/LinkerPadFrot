import React, { Component } from 'react'
import ProjectTableRowComponent from './ProjectTableRowComponent'


class ProjectTableComponent extends Component {


    render() {

        const { projectList } = this.props

        if(!projectList) {
            return <h4 className="my-5 dir-rtl text-center">پروژه‌ای ثبت نشده است</h4>
        }

        return (
            <div className="table-responsive">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th className="dir-rtl text-center w-10"></th>
                            <th className="dir-rtl text-right w-30">پروژه</th>
                            <th className="dir-rtl text-center w-10">تعداد اعضا</th>
                            <th className="dir-rtl text-center w-10">سطح دسترسی</th>
                            <th className="dir-rtl text-center w-20">تاریخ شروع</th>
                            <th className="dir-rtl text-center w-10">کد پروژه</th>
                            <th className="dir-rtl text-center w-10"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <ProjectTableRowComponent projectList={projectList} />
                    </tbody>
                </table>
            </div>
        )
    }

}


export default ProjectTableComponent