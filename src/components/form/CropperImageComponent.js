import React, { Component } from "react";
import Cropper from "react-easy-crop";
import { connect } from "react-redux";
import { setCroppedAreaPixelsOfImage } from "../../actions/optionAction";

export class CropperImageComponent extends Component {
  constructor(props) {
    super(props);
    const { imageUrl } = this.props;
    this.state = {
      imageUrl,
      zoom: 1,
      aspect: 4 / 4,
      crop: { x: 0, y: 0 }
    };
  }

  componentWillReceiveProps(newProps) {
    const { imageUrl } = newProps;
    if (imageUrl !== this.state.imageUrl) {
      this.setState({ imageUrl });
    }
  }

  onCropChange = crop =>
    this.setState({ crop });

  onZoomChange = zoom =>
    this.setState({ zoom });

  onCropComplete = (croppedArea, croppedAreaPixels) =>
    this.props.setCroppedAreaPixelsOfImage(croppedAreaPixels);

  render() {
    const cropper = this.state;

    return (
      <Cropper
        image={cropper.imageUrl}
        crop={cropper.crop}
        zoom={cropper.zoom}
        aspect={cropper.aspect}
        style={{ cropAreaStyle: { color: "#ffffff82" } }}
        onCropChange={this.onCropChange.bind(this)}
        onZoomChange={this.onZoomChange.bind(this)}
        onCropComplete={this.onCropComplete.bind(this)}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  setCroppedAreaPixelsOfImage: croppedAreaPixels =>
    dispatch(setCroppedAreaPixelsOfImage(croppedAreaPixels))
});

export default connect(
  null,
  mapDispatchToProps
)(CropperImageComponent);
