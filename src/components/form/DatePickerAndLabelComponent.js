import React, { Component } from 'react'
import DatePicker from 'react-datepicker2'
import 'react-datepicker2/dist/react-datepicker2.min.css'


class DatePickerAndLabelComponent extends Component {


    render() {

        const { name, label, value, onChange, isGregorian=false, timePicker=false} = this.props

        return (
            <div className="form-group">
                <label htmlFor={name} className="dir-rtl text-right">{label}:</label>
                <DatePicker
                    id={name}
                    name={name}
                    isGregorian={isGregorian}
                    timePicker={timePicker}
                    className="form-control dir-rtl text-right"
                    value={value}
                    onChange={onChange}
                />
            </div>
        )
    }

}


export default DatePickerAndLabelComponent