import React, { Component } from 'react'
import { connect } from 'react-redux'
import { toggleDisplayCropImageModalComponent, setCroppedImageUrl, setCroppedAreaPixelsOfImage, setPictureName, redirectTo, toggleDisplayFileBrowserForProjectPicture } from '../../actions/optionAction'
import CropperImageComponent from './CropperImageComponent'
import cropImage from '../../utils/cropImage'
import GalleryController from '../../controllers/GalleryController'
import ProjectController from '../../controllers/ProjectController'
import * as validationMessage from '../../utils/validationMessage'


class CropImageModalComponent extends Component {


    constructor(props) {
        super(props)
        this.state = { blobImageUrl: '' }
    }


    componentDidMount() { this.handleShowModal(this.props) }


    componentWillReceiveProps(newProps) { 
        this.handleShowModal(newProps) 
        const { projectImage } = newProps
        if(newProps.projectImage && newProps.projectImage !== this.state.projectImage) {
            const blobImageUrl = URL.createObjectURL(newProps.projectImage)
            this.setState({ blobImageUrl, projectImage })
        }
    }


    handleShowModal = ({ displayCropImageModalStatus }) => {
        if(displayCropImageModalStatus) {
            this.refs.cropImageModal.className = "modal fade show d-block"
        } else {
            this.refs.cropImageModal.className = "modal fade"
        }
    }


    handleHideModal = () => { 
        this.props.handleSetCroppedAreaPixelsOfImage(null)
        this.props.handleDisplayCropImageModal(false) 
    }


    handleCropAndUploadImage = async () => {
        const { blobImageUrl } = this.state
        const { croppedAreaPixelsOfImage } = this.props
        const { croppedImageUrl, croppedImageFile } = await cropImage(blobImageUrl, croppedAreaPixelsOfImage)

        if(this.props.editProjectId) {
            // edit project mode
            ProjectController.uploadProjectPicture(croppedImageFile, this.props.editProjectId)
                .then(() => {
                    this.props.handleSetCroppedImageUrl(croppedImageUrl)
                    this.handleHideModal()
                })
                .catch(error => {
                    if(error.httpStatus === 401) {
                        this.props.redirectTo(true, '/login', {statusCode:401, message: validationMessage.error("UnathorizedOnServer")})
                    }
                })
        } else {
            // create project mode
            GalleryController.uploadPicture(croppedImageFile)
                .then(result => {
                    const { pictureName } = result.responseObject
                    this.props.handleSetPictureName(pictureName)
                    this.props.handleSetCroppedImageUrl(croppedImageUrl)
                    this.handleHideModal()
                })
                .catch(error => {
                    if(error.httpStatus === 401) {
                        this.props.redirectTo(true, '/login', {statusCode:401, message: validationMessage.error("UnathorizedOnServer")})
                    }
                })
        }
        
    }


    render() {

        const { blobImageUrl } = this.state
        
        return (
            <div id="cropImageModal" tabIndex="-1" ref="cropImageModal">

                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header dir-rtl">
                            <h5 className="modal-title" id="cropImage">تغییر سایز عکس پروژه</h5>
                            <button type="button" className="close" onClick={ this.handleHideModal.bind(this) }>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body" style={{ height:"50vh" }}>
                            <CropperImageComponent imageUrl={ blobImageUrl } />
                        </div>
                        <div className="modal-footer p-0 mt-3">
                            <div className="col px-0 mx-0">
                                <button 
                                    type="button" 
                                    className="btn btn-secondary h-100 w-100 border-radius-none text-white" 
                                    data-dismiss="modal" 
                                    onClick={ this.handleHideModal.bind(this) }>
                                        لغو
                                </button>
                            </div>
                            <div className="col px-0 mx-0">
                                <button 
                                    type="button" 
                                    className="btn btn-primary h-100 w-100 border-radius-none" 
                                    data-dismiss="modal" 
                                    onClick={ () => {this.props.handleDisplayFileBrowserForProjectPicture(true)} }>
                                        انتخاب عکس جدید
                                </button>
                            </div>
                            <div className="col px-0 mx-0">
                                <button 
                                    type="button" 
                                    className="btn btn-success h-100 w-100 border-radius-none" 
                                    onClick={ this.handleCropAndUploadImage.bind(this) }>
                                        ثبت
                                </button>
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}


const mapStateToProps = state => ({
    displayCropImageModalStatus: state.options.displayCropImageModalComponent,
    projectImage: state.options.projectImageFileForModal,
    croppedAreaPixelsOfImage: state.options.croppedAreaPixelsOfImage,
    editProjectId: state.options.editProjectId
})


const mapDispatchToProps = dispatch => ({
    handleDisplayCropImageModal: status => dispatch(toggleDisplayCropImageModalComponent(status)),
    handleSetCroppedImageUrl: croppedImageUrl => dispatch(setCroppedImageUrl(croppedImageUrl)),
    handleSetPictureName: pictureName => dispatch(setPictureName(pictureName)),
    handleSetCroppedAreaPixelsOfImage: 
        croppedAreaPixelsOfImage => dispatch(setCroppedAreaPixelsOfImage(croppedAreaPixelsOfImage)),
    handleDisplayFileBrowserForProjectPicture: status => dispatch(toggleDisplayFileBrowserForProjectPicture(status)),
    redirectTo: 
        (redirectStatus, redirectPathName, redirectState) => dispatch(redirectTo(redirectStatus, redirectPathName, redirectState))
})


export default connect(mapStateToProps, mapDispatchToProps)(CropImageModalComponent)