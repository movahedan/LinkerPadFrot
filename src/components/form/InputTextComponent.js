import React, { Component } from 'react'



class InputTextComponent extends Component {


    constructor(props) {
        super(props)
        this.state = {
            message: {
                text: '',
                status: ''
            }
        }

        this.setMessage = this.setMessage.bind(this)
    }


    setMessage(status='none', text='') {
        const message = { status,text }
        this.setState({ message })
    }


    render() {
        
        const { message } = this.state

        const { name, type='text', value, placeholder, onChange, inputClass, parentInputClass, children } = this.props

        let inputFieldClass=inputClass, feedbackClass=''

        switch(message.status) {
            case 'invalid':
                inputFieldClass += ' is-invalid'
                feedbackClass = 'text-right invalid-feedback'
                break
            case 'valid':
                inputFieldClass += ' is-valid'
                feedbackClass = 'text-right valid-feedback'
                break
            default:
        }

        return (
            <div className={ parentInputClass? parentInputClass: 'col-12' }>
                
                <input 
                    type={type}
                    name={ name } 
                    value={ value } 
                    className={ inputFieldClass } 
                    placeholder={ placeholder } 
                    onChange={ onChange } 
                />

                <div className={feedbackClass}>
                    {message.text}
                </div>

                { children }

            </div>
        )
    }
}


export default InputTextComponent