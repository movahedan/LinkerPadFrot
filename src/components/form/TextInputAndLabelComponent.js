import React, { Component } from 'react'


class TextInputAndLabelComponent extends Component {


    constructor(props) {
        super(props)
        this.state = {
            message: {
                text: '',
                status: ''
            }
        }

        this.setMessage = this.setMessage.bind(this)
    }


    setMessage(status='none', text='') {
        const message = { status, text }
        this.setState({ message })
    }


    render() {

        const { message } = this.state

        const { name, value, label, placeholder='', onChange } = this.props

        let inputFieldClass="form-control dir-rtl text-right ", feedbackClass=''

        switch(message.status) {
            case 'invalid':
                inputFieldClass += ' is-invalid'
                feedbackClass = 'text-right invalid-feedback'
                break
            case 'valid':
                inputFieldClass += ' is-valid'
                feedbackClass = 'text-right valid-feedback'
                break
            default:
        }

        return (
            <div className="form-group">
                <label htmlFor={name} className="dir-rtl text-right">{label}:</label>
                <input 
                    id={name} 
                    name={name} 
                    value={value} 
                    type="text" 
                    placeholder={placeholder}
                    className={inputFieldClass}
                    onChange={onChange}
                />

                <div className={feedbackClass}>
                    {message.text}
                </div>

            </div>
        )
            
    }

}


export default TextInputAndLabelComponent