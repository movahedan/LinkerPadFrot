import React, { useEffect, useRef, useContext } from "react";
import { DatePicker } from "jalali-react-datepicker";
import moment from "moment-jalaali";
import useTimeContext from "./../../views/project/ProjectDetailView/useTimeContext";

import { Button } from "react-bootstrap";
import "./style.scss";

moment.loadPersian({
  dialect: "persian-modern",
  usePersianDigits: true
});

export default () => {
  const { value, setValue, nextDay, prevDay, toDay } = useTimeContext();
  const datePickerInputElement = useRef(null);
  useEffect(() => {
    datePickerInputElement.current = document.getElementsByClassName(
      "dp__input"
    )[0];
  }, []);

  return (
    <section className="ml-auto mr-3 my-1 date-picker-wrapper">
      <DatePicker
        timePicker={false}
        value={new Date(value)}
        onClickSubmitButton={e => setValue(e.value.lang("fa"))}
      />
      
      <div className="text-center my-2">
      <Button
        variant="outline-dark"
        className="circular ml-3"
        onClick={toDay}
      >
        امروز
      </Button>
        {/* <span
          className="px-1 customized-arrow-button"
          onClick={() => setValue(moment(value).add(1, "months"))}
        >
          <i className="fas fa-angle-double-right fa-lg vertical-align-middle" />
        </span> */}
        <span
          className="px-1 customized-arrow-button"
          onClick={nextDay}
        >
          <i className="fas fa-angle-right fa-lg vertical-align-middle" />
        </span>
        <span
          className="date-picker-customized-button mr-auto align-self-center px-2 cursor-pointer"
          onClick={() => datePickerInputElement.current.click()}
        >
          <i className="fas fa-calendar-alt fa-lg vertical-align-middle ml-2" style={{color: "#4679ae"}} />
          {value.format("dddd jDD jMMMM jYY")}
        </span>
        <span
          className="px-1 customized-arrow-button"
          onClick={prevDay}
        >
          <i className="fas fa-angle-left fa-lg vertical-align-middle" />
        </span>
        {/* <span
          className="px-1 customized-arrow-button"
          onClick={() => setValue(moment(value).add(-1, "months"))}
        >
          <i className="fas fa-angle-double-left fa-lg vertical-align-middle" />
        </span> */}
      </div>
    </section>
  );
};
